function [ points, fileInfo ] = read_points( directory, root, ext)
% READ_POINTS reads a folder of .pts files for analysis
% function [ points ] = read_points( directory, extension )
%  For FaceModelGUI files, use [points, files] = READ_POINTS(<directory>)
%   directory includes the directory name to be read
%     read_points will read all files in that directory
%     with the appropriate root and extension
%   root is the part of the file name before the number.  defaults to
%     'frame'
%   ext is the file extension to read. Defaults to '.pts'

narginchk(1, 3);

if nargin < 3
    ext='.pts';
end
if nargin < 2
    root='frame';
end

% Get file list
flist = dir( fullfile(directory, [root '*' ext]));
% And sort numbers
fnums = zeros(length(flist),1);
%for i = 1:length(flist)
%    fnums(i) = sscanf(flist(i).name, [root '%d' ext]);
%end

[fnums, ind] = sort(fnums);
fnames = {flist.name};
fnames = fnames(ind);
fullfile(directory,fnames{1})
% Get sizes
pts = importdata(fullfile(directory, fnames{1}), ' ', 2);

points = zeros([size(pts.data) length(fnames)]);

for i = 1:length(fnames)
    % Actual file read here.
    tFile = importdata(fullfile(directory,fnames{i}), ' ', 2);
    points(:,:,i) = tFile.data;
end

fileInfo.name = fnames';
fileInfo.number = fnums;

end

