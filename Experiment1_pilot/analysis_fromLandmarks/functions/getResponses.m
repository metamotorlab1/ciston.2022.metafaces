

function getResponses(subjs_numIndex, myExpressions)



global subjects_of_interest
global inputDir inputDirExternalRatings
global expressionIndexes
global allSubjs_aggregated


%%
allSubjs_aggregated.rater2 = [];
allSubjs_aggregated.rater3 = [];
allSubjs_aggregated.rater4 = [];
allSubjs_aggregated.rater5 = [];
allSubjs_aggregated.rssq_distance = [];
allSubjs_aggregated.RTrating = [];
allSubjs_aggregated.RTtype1 = [];
allSubjs_aggregated.confidence = [];
allSubjs_aggregated.trialNumber = [];
allSubjs_aggregated.subjNumber = [];
allSubjs_aggregated.expression = [];
allSubjs_aggregated.repetition = [];


for this_subj = subjs_numIndex
    
    load([inputDir filesep 'preprocessed_5points' subjects_of_interest{this_subj} '.mat'],  '-regexp', '^(?!inputDir)\w')
    
    
    %originalStimNum = num(29:58,2);
    %originalStimNum = num(34:63,2); %%for subjects 206 & 207
    %originalStimNum is the number of black-background expression ie, the number of the picture used as an initial stimulus.
    originalStimNum = num( find( ismember( txt(:,1), 'nextblock' ), 1):end, 2);
    
    originalStimNum(isnan(originalStimNum)) = [];
    
    %Because not everybody got the same stimuli in the same order, and
    %because we may want to analyze exactly which stimuli we want, we need
    %to find the expressionIndexes (i.e., the rows in picturePairs) that
    %correspond, in this particular subject, to the black-background
    %expressions we're interested in.
    %Then, we need an indexing system used for all subjects, to store
    %distances and RTs. It will all be referenced to the real,
    %black-background expression, in order to allow to compare all this
    %across subjects. i.e. expressionIndexes
    
    expressionIndexes = find( ismember( originalStimNum, myExpressions))';
    
    %% distance - procrustes: rotated and target faces
    iExp = 1;
    for expr = expressionIndexes
        originalExp = myExpressions(iExp);
        for r = 1:8
            %                                             \delta_x.y^2 = ((x_target,y_target)-(x_resp,y_resp)).^2
            rssq_distance{originalExp,r,this_subj} = rssq(rssq(preprocessed.procrustesAligned{expr,r+1} - preprocessed.procrustesAligned{expr,1}, 2)); %or sum(rssq)
            allSubjs_aggregated.rssq_distance = [allSubjs_aggregated.rssq_distance; cell2mat(rssq_distance(originalExp,r,this_subj))];
        end
        iExp = iExp + 1;
    end
    
    
    %% similarity ratings from external judges
    %rater 1 did not complete all expressions
    for rater = 2:5
        load([inputDirExternalRatings '/Rater' num2str(rater) filesep subjects_of_interest{this_subj}(1:3) '_rating_0' num2str(rater) '.mat'])
        for frame = 1:length(out)
            %find the corresponding original expression in picturePairs
            [originalExp, r] = find(picturePairs == out(frame).frame);
            thisRaterDistance(originalExp, r, this_subj, rater) = out(frame).answer;
        end
        
        for expr = expressionIndexes
            for r = 1:8
                allSubjs_aggregated.(['rater' num2str(rater)]) = [allSubjs_aggregated.(['rater' num2str(rater)]); thisRaterDistance(expr,r+1,this_subj,rater)];
            end
        end
    end
    
    %% get the RTs and confidences, for each expression and response
    ratingCol = 10;
    iExpr = 1;
    for expr = expressionIndexes
        originalExp = myExpressions(iExpr);
        responses  = picturePairs(iExpr,2:end);     %picturePairs is built from the data in xls sheet
        responses = responses + 1 ;                 %because picturePairs starts with 0 but frameNum starts with 1.
        
        for r = 1:length(responses)
            % collect subjective judgement
            thisSubjJudge = str2double( txt{find(frameNum==responses(r)),ratingCol} );
            if isnumeric(thisSubjJudge)
                confidence(originalExp,r,this_subj) = thisSubjJudge;
            else
                confidence(originalExp,r,this_subj) = NaN;
            end
            % collect RT
            RTrating(originalExp,r,this_subj) = num(find(frameNum==responses(r)),RT_ratingCol);
            RTtype1(originalExp,r,this_subj) = num(find(frameNum==responses(r)),RT_imageCol);
            allSubjs_aggregated.RTrating = [allSubjs_aggregated.RTrating; RTrating(originalExp,r,this_subj)];
            allSubjs_aggregated.RTtype1 = [allSubjs_aggregated.RTtype1; RTtype1(originalExp,r,this_subj)];
            allSubjs_aggregated.confidence = [allSubjs_aggregated.confidence; confidence(originalExp,r,this_subj)];
            allSubjs_aggregated.subjNumber = [allSubjs_aggregated.subjNumber; this_subj];
            allSubjs_aggregated.expression = [allSubjs_aggregated.expression; originalExp];
            allSubjs_aggregated.repetition = [allSubjs_aggregated.repetition; r];
        end
        iExpr = iExpr + 1;
    end
    
    %a couple of calculations to match picturePairs with trialNumbers (as in file names)
    theTrialNumbers = picturePairs(:,2:9); %exclude target-generation trials.
    
    %picturePairs starts 0, trial Numbers starts with 1. so remove the first 30 target-generation trials,
    %add 2000, to mark that it's block 2 (wich is indicated in the file name
    theTrialNumbers = theTrialNumbers +1;
    theTrialNumbers(:,1:4) = theTrialNumbers(:,1:4) - 30 + 2000;
    
    %same for block 3: remove 30 +120 = 150 trials and add 3000 tag to indicate that it's block 3
    theTrialNumbers(:,5:8) = theTrialNumbers(:,5:8) - 150 + 3000;
    
    allSubjs_aggregated.trialNumber = [allSubjs_aggregated.trialNumber; theTrialNumbers(:)];
    
end

allSubjs_aggregated.trialNumber = allSubjs_aggregated.trialNumber(:);


end

