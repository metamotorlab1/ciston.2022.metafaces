# Ciston.2022.MetaFaces

Data, analysis codes, results, and scripts for the paper by Anthony B. Ciston, Carina Forster, Timothy R. Brick, Simone Kühn, Julius Verrel, Elisa Filevich,
Do I look like I'm sure?: Partial metacognitive access to the low-level aspects of one's own facial expressions,
Cognition, Volume 225, 2022, 105155, ISSN 0010-0277, https://doi.org/10.1016/j.cognition.2022.105155.


https://www.sciencedirect.com/science/article/abs/pii/S0010027722001433 

This is a copy of the repository that was linked in the paper. 

