clear all;
close all;
commandwindow;

%settings for confidence scale
global cfg
global dev 

%basic settings
tic; 
Screen('Preference', 'SkipSyncTests', 0); %0 for real run, 1 debug
whichscreen = 0;
screeninfo = Screen('Resolution', whichscreen);
screencentre = [screeninfo.width/2 screeninfo.height/2];
KbName('UnifyKeyNames');   
cfg.itt = 0.2;
cfg.bckgnd = 0 * [1 1 1]; %black background
cfg.matlabFileName  = '%id%-%date%.mat';
cfg.dateFormat  = 'ddmmyy-HHMMSS';
cfg.id   = input('Subject Initials: ', 's');


cfg.debug = 0;

% fix PTB Java Trouble, apparently
% PsychJavaTrouble;

%Path updated to share publicly
rootPath        = '/Users/metamotorlab/faces_experiment'; 
cfg.dataPath    = '/Users/metamotorlab/faces_experiment/test_data/'; 


functionsPath   = '/functions';
addpath(rootPath);
addpath([rootPath functionsPath]); 
cd(cfg.dataPath);   

  
%define base folder
cfg.tablefolder = ['Users' filesep 'metamotorlab' filesep 'Dropbox' filesep 'Hiwis' filesep 'Carina' filesep 'faces_experiment' filesep 'test_data'];
cfg.subjIDlength = 5; %If not 5 loading images will fail

%get subject folder from rater
%cfg.imgfolder = uigetdir(cfg.tablefolder, 'Open subject folder');

% %get subject name 
% subj = regexp(cfg.imgfolder, '\d\d\d\S\S', 'match'); %matches 3 digits plus 2 characters (participant ID)
% 
% %get subject number as string
% %subj = regexp(cfg.imgfolder, '\d\d\d', 'match');
% 
% %check
% if( isempty(subj) )
%      error('Invalid subject folder. Name must be of the form ''##''.');
%  else
%      subj = subj{1};
% end;

%get preprocessed file (for picturePairs) from rater
% [ppfname, pppname] = uigetfile( fullfile( cfg.tablefolder, ['*' subj '*.mat'] ), 'Select Preprocessing file');
% cfg.ppfile = fullfile  (pppname, ppfname);
% 
% cfg.savefile = fullfile(pppname, [subj '_rating.mat' ] );

%get preprocessed file (for picturePairs) from rater, wth subject ID

[ppfname, pppname] = uigetfile( fullfile( cfg.tablefolder, '*.mat' ), 'Select Preprocessing file');
cfg.ppfile = fullfile(pppname, ppfname);
cfg.savefile = fullfile(pppname, 'rating.mat');

% %define keys allowed for rating ( play around with KbName and KbCheck to find that out )
% if ismac
%     cfg.ratekeys = [30:35 38]; %# what is unifykeynames good for again? 
% elseif ispc
%     cfg.ratekeys = 49:54;
% end
  
%load picturePairs table 
load(cfg.ppfile, 'data');
    
 
for block = 2:size(data,1) % we start on block 2 because block 1 is just the response to the actors' faces
    for trial = 1:size(data{block},1) 
        picturePairs(block,trial) = data{block}{trial}.getpicturefromtrial;
    end 
end;    

%%
dev.break = KbName('b');

%saved data
%  data.out = struct('answer', [], 'time   ', []); do we need the time? 
 

if cfg.debug == 1
    PsychDebugWindowConfiguration;
    [dev.wnd, dev.screenRect] = Screen('OpenWindow', whichscreen, 0, [0 0 800 600]);
else
    [dev.wnd, dev.screenRect] = Screen('OpenWindow', whichscreen, 0);
end;

%VAS settings    

%Get mouseId
dev.mouseId = GetMouseIndices;
dev.mouseId = dev.mouseId(1); 

cfg.txtCol  = 255 * [1 1 1];
cfg.cursorCol = 175 * [1 0 1]; %red cursor
dev.xCenter = (dev.screenRect(3) - dev.screenRect(1))/2 + dev.screenRect(1);
dev.yCenter = (dev.screenRect(4) - dev.screenRect(2))/2 + dev.screenRect(2);

% for vertical scale
cfg.maxConfRT          = Inf;            %secs
cfg.VAS.bandWidth      = 20;
cfg.VAS.stringLow      = '-';
cfg.VAS.stringHigh     = '+';
%cfg.VAS.questionString = 'Wie sicher?';
cfg.VAS.nBands         = 10;
cfg.VAS.bandLength     = round(dev.yCenter/7); %7?    %width of each colour band in the confidence scale
cfg.VAS.colorLight     = round(1/2 * 255 * [1 1 1]);
cfg.VAS.colorDark      = 255 * [1 1 1];

% % for horizontal scale
% cfg.maxConfRT          = Inf;            %secs
% cfg.VAS.bandHeight     = 20;collectVASjudge_vertical_img
% cfg.VAS.stringLeft     = '-';
% cfg.VAS.stringRight    = '+';
% cfg.VAS.questionString = 'Wie sicher?';
% cfg.VAS.nBands         = 10;
% cfg.VAS.bandWidth      = round(dev.xCenter/7); %7?    %width of each colour band in the confidence scale
% cfg.VAS.colorLight     = round(1/2 * 255 * [1 1 1]);
% cfg.VAS.colorDark      = 255 * [1 1 1];

%true as soon as rater presses escape
dev.break = false;
HideCursor;

for b = 2:size(picturePairs,1)
    for t=1:numel(picturePairs(b,:))
        
       subjectName = ppfname(1:cfg.subjIDlength); 
       
       %refimg = data{1}{picturePairs(b,t)}.fileNamePicture;
       
       refimg = dir([pppname, subjectName '-' num2str(1) '-' num2str(picturePairs(b,t)) '-*.jpg']);
       
       rateimg = dir([pppname, subjectName '-' num2str(b) '-' num2str(t) '-*.jpg']);
        
       reft = readBitmap(dev.wnd, [pppname refimg.name]);
       ratet = readBitmap(dev.wnd, [pppname rateimg.name]); 
       
%        reft = readBitmap(dev.wnd, refimg);
%        ratet = readBitmap(dev.wnd, rateimg);
       
       Screen('Flip', dev.wnd);  
       
       %collectVASjudge_img(block, trial, reft, ratet);
       
       VAS_data = collectVASjudge_vertical_img([], [],reft, ratet);
       
       ratedata{b}{t}.VASrateimg = VAS_data.VAS;
       
       Screen('Close', reft);
       Screen('Close', ratet);
       
       if dev.break      
          break;   
       end
        
       Screen('Flip', dev.wnd);  
       sprintf('done with block %d, trial %d', b, t)
       
       pause( cfg.itt );
        
    end
    
    if dev.break      
        break;   
    end
end

matName = [cfg.dataPath filesep strrep_struct(cfg.matlabFileName, struct( 'id', cfg.id,...
                   'date', datestr(now, cfg.dateFormat )) )];

save(matName, 'ratedata');

%save(cfg.savefile, 'ratedata');

Screen('Close', dev.wnd );