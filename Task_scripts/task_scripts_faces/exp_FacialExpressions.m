 
clear all; %#ok<*CLALL>




global dev;
global cfg;
global data;
global frameIndex;


rng('default');


%paths updated for public link
rootPath        = pwd; 
cfg.dataPath    = pwd;  

functionsPath   = '/functions/';
addpath(rootPath)
addpath([rootPath functionsPath])


%% test timing
% https://de.mathworks.com/help/supportpkg/arduinoio/examples/getting-started-with-matlab-support-package-for-arduino-hardware.html?prodcode=ML
%to test timing, set timing test to equal 1 and connect the arduino
%(package needs to be installed on that PC. the arduino is set to 90ms (so
%the LED at the arduino is on for 90 ms, you should see the LED realiably in
%the images taken, if not change the interval in functions,          
%showandtakePicture line 89ff, 
%images for 80,90 and 100ms are in
%the DATA folder (faces_task)

cfg.timingTest = 0;
if cfg.timingTest
    dev.arduinoHandle = arduino();
    
    %Blink tiny built-in LED
    writeDigitalPin(dev.arduinoHandle, 'D13', 1);
   %  writeDigitalPin(dev.arduinoHandle, 'D13', 0); %uncomment to see the
   %  blinking LED
end
%% init
AssertOpenGL;
runaftercrash = 0;

% Test if we're running on PTB-3, abort otherwise
KbName('UnifyKeyNames');

id           = input('Subject Initials: ', 's');% use participant ID, to make sure rating script works (3 numbers, 2 characters)
training =input('Training?, 1=yes: ' );
realexperiment = input('Realrun?, 1=yes: ');
%runaftercrash = input('RunningAfterCrash?, 0=no: ');
% if runaftercrash
%     startblock = input('startBlock? ');
%     starttrial = input('startTrial? ');
% else 
%     startblock = 1;
%     starttrial = 1;
% end

cfg.dataPath = [cfg.dataPath filesep id];
mkdir(cfg.dataPath); 
cd(cfg.dataPath);               % VideoRecording crashes if you just save in this directory instead of cd'ing
startTime    = GetSecs();
frameIndex = 0;


if runaftercrash
    [dataFilename, pathToData] = uigetfile( fullfile(cfg.dataPath, '*.mat' ), 'Select data file');
end

%%
%training = 1;
%realexperiment = 1; 

if realexperiment == 1
    cfg.debug = 0;
    cfg.confidence = 1;
    cfg.pathToImages = [rootPath '/TestImages']; %all images will be shown
    cfg.blocks              = 1 + 4 ; % 1st block to record subject-target images, 4 blocks to show them and get responses ~ 256 trials in total
    cfg.repetitionsPerBlock = 2; % 2 == about 10 mins per block, probably less
    Screen('Preference', 'SkipSyncTests', 0);
elseif training ==1
    cfg.debug = 0;
    cfg.confidence = 0;
    cfg.pathToImages = [rootPath '/TrainImages'];
    cfg.blocks = 1;
    cfg.repetitionsPerBlock = 1;
    id = [id '_training'];
    Screen('Preference', 'SkipSyncTests', 0);
else
    cfg.debug = 1;
    cfg.confidence = 1;
    cfg.pathToImages        = [rootPath '/TestImages_short'];%All images in this folder will be used
    cfg.blocks              = 1 + 1 ; % 1st block to record subject-target images, 4 blocks to show them and get responses ~ 256 trials in total
    cfg.repetitionsPerBlock = 1; % 2 == about 10 mins per block
    Screen('Preference', 'SkipSyncTests', 1);
end

%% General settings
%ListenChar(2);
cfg.escapeChar  = KbName('escape');
cfg.breakChar   = KbName('b');
cfg.startChar   = KbName('s');
cfg.confirmChar = KbName('space');
dev.kbDevice    = [];                       %leave empty for now, reconsider if you have multiple
cfg.dateFormat  = 'ddmmyy-HHMMSS';
cfg.eyetrack    = 0;
cfg.bckgnd      = 0 * [1 1 1];
screen = max(Screen('Screens'));            % Open window on secondary display, if any


cfg.runCalibration      = 1;


cfg.expressions         = dir([cfg.pathToImages '/*.jpeg']); 


cfg.maxType1RT          = Inf;   % secs

%Read in trial (starting image) order from an xls config file, or randomize 
%fully within this function
data = ReadInOrRandomizeTrials;


 
%% Open PTB window
%
if cfg.debug == 1
    PsychDebugWindowConfiguration;
    PsychImaging('PrepareConfiguration');
    PsychImaging('AddTask', 'AllViews','FlipHorizontal')
    %[dev.wnd, dev.rect] = PsychImaging('OpenWindow', screen, 1);
    [dev.wnd, dev.rect]= PsychImaging('OpenWindow', screen, 0, [0 0 800 600]); %took dev.rect
else
% 
%     %these three commands replace Screen(Openwindow)
%     PsychImaging('PrepareConfiguration');
%     PsychImaging('AddTask', 'AllViews','FlipHorizontal')
%     [dev.wnd, dev.rect] = PsychImaging('OpenWindow', screen, 1); %flips images as well as camera image left is on the right and vice versa, VAS works without that setting? Why? flip yes or no?
    [dev.wnd, dev.rect] = Screen('OpenWindow', screen, 1); %no flip
end;

%away
%working screen
%[dev.wnd, dev.rect] = Screen('OpenWindow', screen, 1); %    second screen, change resolution to 960 x1280, maybe higher or lower, press option (alt) key to change resolution on second screen, ask ELisa if that changes sth?
Screen('FillRect', dev.wnd, cfg.bckgnd);
% Screen('glRotate', dev.wnd, 90);
Screen('Flip',dev.wnd);             % Initial flip to a blank screen
Screen('TextSize', dev.wnd, 36);    % Set text size for info text. 24 pixels is also good for Linux.
%Screen('glRotate', dev.wnd, angle, [rx=0], [ry=0] ,[rz=1]); not needed,
%go to second screen, system preferences, screen, mirror screen, rotate 270
%degrees, don't forget to plug second screen in and change screen
%preferences to 1   


%% Config video recording and display settings
%video
cfg.calibrateMovieName = id;
cfg.videoFlags      = 1; %1 should store the video in memory and only then save. Increases framerate
cfg.pixeldepth      = 0; %1 will give you luminance, black-white videos
cfg.codec           = ':CodecType=DEFAULTencoder';
cfg.waitforimage    = 1;
cfg.videoFlags      = cfg.videoFlags + 64;   % Use +64 to always request timestamps in movie recording time instead of GetSecs() time
%result image/moviename includes full start image name
cfg.movieFileName   = '%id%-%block%-%trial%-%stimuli%-%date%.avi';          
cfg.pictureFileName = '%id%-%block%-%trial%-%stimuli%-%date%.jpg';
cfg.matlabFileName  = '%id%-%date%.mat';
cfg.vcFPS           = 30;


%Open video capture. You'll need it for either callibration or actual
%trials
dev.vcDevice = Screen('OpenVideoCapture', dev.wnd, [], [], cfg.pixeldepth, [], [], cfg.codec, cfg.videoFlags, [], 8);
WaitSecs('YieldSecs', 1);

%Note: The sequence for controlling video is the following. Anything else
%will give a difficult to understand error message:
% OpenVideoCapture - (SetVideoCaptureParameter) 
% StartVideoCapture - StopVideoCapture
% ... n times
% StartVideoCapture - StopVideoCapture
% CloseVideoCapture

%display
cfg.txtCol  = (1/2 * 255 * [1 1 1]); % original colour 255 * [1 1 1];
dev.xCenter = ((dev.rect(3) - dev.rect(1))/2 + dev.rect(1));
dev.yCenter = ((dev.rect(4) - dev.rect(2))/2 + dev.rect(2));


%% Confidence settings

cfg.cursorCol = 255 * [1 0 0];
cfg.maxConfRT           = Inf;            %secs
cfg.VAS.bandWidth      = 20; %original height: 20
cfg.VAS.stringLow      = '-';
cfg.VAS.stringHigh     = '+';
% cfg.VAS.questionString  = 'Wie gut konntest du den Gesichtsausdruck imitieren?';
cfg.VAS.nBands          = 10;
cfg.VAS.bandLength       = round(dev.yCenter/7);     %width of each colour band in the confidence scale
cfg.VAS.colorLight      = round(1/2 * 255 * [1 1 1]);
cfg.VAS.colorDark       = 255 * [1 1 1];
dev.mouseId = GetMouseIndices;
dev.mouseId = dev.mouseId(1);

%% Record video with online feedback to callibrate
% All this is very well documented in VideoRecordingDemo. Check there

x=100; %coordinates for mirroring text
y=100;

if cfg.runCalibration
    getCalibrationVideo; %just a script, no need for globals or input params
end

%% main Loop

Screen('Flip',dev.wnd);             % Flip to a blank screen

frame = 1;

if ~runaftercrash
    matName = [cfg.dataPath filesep strrep_struct( cfg.matlabFileName, struct( 'id', id,...
                   'date', datestr(now, cfg.dateFormat )) )];
else
    dataFromRunThatCrashed = fullfile(pathToData, dataFilename);
    
    matName = [cfg.dataPath filesep strrep_struct( cfg.matlabFileName, struct( 'id', id,...
                   'date', datestr(now, cfg.dateFormat )) )];
end


if runaftercrash
    load(dataFromRunThatCrashed)
end

startblock = 1;
starttrial = 1;

for block = startblock : cfg.blocks
     
    Screen('TextSize', dev.wnd, 72);    %original 24

    %draw information before block
%     DrawFormattedText(dev.wnd, ['Block Nummer '  num2str(block)], 'center', dev.yCenter - 50, cfg.txtCol, [], [1]);
%     DrawFormattedText(dev.wnd, 'Versuche die gezeigte Mimik so gut wie moeglich zu imitieren.', 'center', dev.yCenter, cfg.txtCol, [], [1]);
%     DrawFormattedText(dev.wnd, 'Druecke die Leertaste.', 'center', dev.yCenter + 50, cfg.txtCol, [], [1]);
%     DrawFormattedText(dev.wnd, 'Druecke bitte die Taste S um den Block zu beginnen.', 'center', dev.yCenter+100, cfg.txtCol, [], [1]);
    DrawFormattedText(dev.wnd, '+', 'center', dev.yCenter+100, cfg.txtCol, [], [1]);
    Screen('Flip', dev.wnd);
    
    %wait for input (not timed) to begin block
    RestrictKeysForKbCheck([cfg.startChar, cfg.breakChar])
    commandwindow;
    KbPressWait;
    RestrictKeysForKbCheck([]);
    

   
    for trial = starttrial : numel(data{block})
        
        Screen('Flip', dev.wnd);                %Flip blank
         
        if ~isnan(data{block}{trial}.getpicturefromblock)       
  
            %display previously recorded picture
            %This command won't work for the 'emergency/recovery' version of the script (as it currently is) because it takes
            %a pointer to an off-screen buffer where the corresponding
            %picure is stored. The simplest way around it would be to give
            %the path to the corresponding image, but that interferes with
            %the way that showAndTakePicture realizes that you're on block
            %1, where the path to the initial images is given. So, we
            %forget about the emergency option for now and hope that the
            %code is robust enough so that we don't use it. 
            showAndTakePicture(id, block, trial, data{data{block}{trial}.getpicturefromblock}{...
												   data{block}{trial}.getpicturefromtrial }.picture );  
                                                                       
                                               
            %Collect confidence judgement 
            
            if cfg.confidence
                 VAS_data = collectVASjudge_vertical();
                 data{block}{trial}.VASrealtask = VAS_data.VAS;        
                 %vertical VAS, from Christinas folder, metasensory experiment

            end
                                               
        else
            %display a given image from the set of stimuli images
            showAndTakePicture(id, block, trial, cfg.expressions(data{block}{trial}.facialexpression).name);
            
            %Collect confidence judgement here as well? was one thought at some point
            %collectVASjudge(block, trial);
             
        end
        sprintf('done with block %d, trial %d', block, trial)
    end
    
    save(matName, 'data', 'cfg', 'dev'); % save data after each block

    if cfg.runCalibration
        getCalibrationVideo; %just a script, no need for globals or input params
    end

    
end

%% close

%ListenChar(0);
Screen('TextSize', dev.wnd, 48); %24
DrawFormattedText(dev.wnd, '!', 'center','center', cfg.txtCol, [], [1]); %Vielen Dank
% DrawFormattedText(dev.wnd, 'Druecke bitte eine beliebige Taste um das Experiment zu beenden.', 'center',dev.yCenter + 40, cfg.txtCol, [], [1]);
Screen('Flip', dev.wnd );
pause

%% 
Screen('CloseVideoCapture', dev.vcDevice);  % Close engine 
Screen('CloseAll');
