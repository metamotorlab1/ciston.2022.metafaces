function showAndTakePicture( id, block, trial, stimuli )
global dev;
global cfg;
global data;
global frameIndex;


%draw stimuli
if ischar( stimuli )
    
    myImageFile = [cfg.pathToImages filesep stimuli];
    ima         = imread(myImageFile);
    Screen('PutImage', dev.wnd, ima);
    
    data{block}{trial}.confRating = NaN;
    data{block}{trial}.confRT     = NaN;
    fexpr = stimuli;
    
else
    % draw a mirrored image
    Screen('glPushMatrix', dev.wnd);
    Screen('glTranslate', dev.wnd, dev.rect(3)/2, dev.rect(4)/2,0);
    Screen('glScale', dev.wnd, 1,1, 1); % not mirrored with 1, mirrored -1
    Screen('glTranslate', dev.wnd, -dev.rect(3)/2, -dev.rect(4)/2,0);
    Screen('DrawTexture', dev.wnd, stimuli );
    Screen('glPopMatrix', dev.wnd);
    
    fexpr = cfg.expressions(data{ data{block}{trial}.getpicturefromblock } ...
                                { data{block}{trial}.getpicturefromtrial }.facialexpression ).name;
end

movieName = strrep_struct( cfg.movieFileName, struct( 'id', id,...
    'block', block,...
    'trial', trial,...
    'stimuli', fexpr,...
    'date', datestr(now, cfg.dateFormat )) );

fullMovieName = sprintf('SetNewMoviename=%s.mov', movieName);

pictureName = strrep_struct( cfg.pictureFileName, struct( 'id', id,...
    'block', block,...
    'trial', trial,...
    'stimuli', fexpr,...
    'date', datestr(now, cfg.dateFormat )) );

 
frameName = ['frame' num2str(frameIndex) '.png'];
frameIndex = frameIndex + 1;

pictureName = [cfg.dataPath filesep pictureName];
frameName   = [cfg.dataPath filesep frameName];

data{block}{trial}.fileNameMovie = fullMovieName;
data{block}{trial}.fileNamePicture = pictureName;

%first thing: start recording
Screen('SetVideoCaptureParameter', dev.vcDevice, fullMovieName );
cfg.vcFPS = Screen('StartVideoCapture', dev.vcDevice, cfg.vcFPS, 1 );

%TODO: UGLY: (setup keyboard queue)
keyMask = zeros(1, numel(KbName('KeyNames')));
keyMask( cfg.confirmChar ) = 1;
keyMask( KbName(cfg.escapeChar) ) = 1;
KbQueueCreate(dev.kbDevice, keyMask);
KbQueueStart(dev.kbDevice);

%flip screen, show stimuli on screen
Screen('Flip', dev.wnd);
startSecs = GetSecs();
if cfg.eyetrack; Eyelink('Message', [num2str(stimuli) ' flipped_ b' num2str(block) ' t' num2str(trial) ]); end

%wait for input from keyboard
KbQueueFlush(dev.kbDevice); 

while GetSecs - startSecs < cfg.maxType1RT
    [pressed, firstPress] = KbQueueCheck(dev.kbDevice);
    
    if pressed
        if cfg.eyetrack; Eyelink('Message', [num2str(stimuli) ' responded_ b' num2str(block) ' t' num2str(trial) ]); end
        if cfg.timingTest; writeDigitalPin(dev.arduinoHandle, 'D13', 1); data{block}{trial}.LEDon = GetSecs(); end %Turn on LED
        %Beeper('high', 0.5, 0.4) no beep pls
        break;
    end
    
    % Wait for 1 msec to prevent system overload:
    WaitSecs('Yieldsecs', 0.001);
end

if firstPress(KbName(cfg.confirmChar)) 
end;

responseTimeFace = min(firstPress(firstPress>0));
% LED is on for 90 ms, should see the LED reliably in every picture taken,
% if not increase Interval
if cfg.timingTest;
     while GetSecs() - responseTimeFace < 0.09; end
    %Turn off LED
    if cfg.timingTest; writeDigitalPin(dev.arduinoHandle, 'D13', 0); data{block}{trial}.LEDoff = GetSecs(); end 
end
        
%clear the screen
Screen('FillRect', dev.wnd, cfg.bckgnd);
Screen('Flip', dev.wnd);

 %wait for another second before stopping the video capture
%if cfg.timingTest; 
 %   startWait = GetSecs(); 
  %  while GetSecs - startWait < 2; end
%end

%capture the last image
[data{block}{trial}.picture, captureTimeStamp] = Screen('GetCapturedImage', dev.wnd, dev.vcDevice); %time stamp here??cpu time? take = cputime
%stop capturing
data{block}{trial}.droppedFrames = Screen('StopVideoCapture', dev.vcDevice);
data{block}{trial}.responseTime  = responseTimeFace - startSecs;
data{block}{trial}.keypress = responseTimeFace;
data{block}{trial}.pictureTime = captureTimeStamp;
data{block}{trial}.cameracap = captureTimeStamp-responseTimeFace;

%release keyboard queue
KbQueueRelease(dev.kbDevice);

%save captured image to file
face = Screen('GetImage', data{block}{trial}.picture);
imwrite( face, pictureName );
imwrite( face, frameName );



end
