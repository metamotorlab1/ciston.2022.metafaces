function t=readBitmap( wnd, filename )
    % t=readBitmap( wnd, filename ) 
    %   read Bitmap into PTB-texture
    %
    %   see also imread, imformats, Screen
    %
    [~,~,ext] = fileparts(filename);
		
    %check if file format is supported...
    if isempty( imformats( ext(2:end) ) )
        warning( ['Format ' ext ' not supported!']);
        t = false;
        return
    end
        
    % read image
    try
        [im, colormap] = imread(filename);
    catch me
        sca;
        rethrow(me);
    end
    
    if ~isempty(colormap)
        %we have a indexed picture
       im = uint8( ind2rgb (im, colormap) * 255 ); 
    end

    if isa(im, 'logical')
        im = double(im);
    end
        
    % load image into video memory
    try
        t = Screen('MakeTexture', wnd, im );
    catch me
        sca;
        rethrow(me);
    end
end