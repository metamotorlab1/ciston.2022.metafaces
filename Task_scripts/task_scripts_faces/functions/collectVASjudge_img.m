 
function collectVASjudge(block, trial, reft, ratet)

% 'Plugin' function to display and record responses to a visual analog
% scale. 
%   
% You'll have to set some configuration parameters in the function calling
% this one. They are:
% 
% Get mouseId
% dev.mouseId = GetMouseIndices;
% dev.mouseId = dev.mouseId(1);
%     
% [dev.wnd, dev.rect] = Screen('OpenWindow', screen, ... your OpenScreen
% command
%
% cfg.txtCol  = 255 * [1 1 1];
% dev.xCenter = (dev.rect(3) - dev.rect(1))/2 + dev.rect(1);
% dev.yCenter = (dev.rect(4) - dev.rect(2))/3 + dev.rect(2);
% 
% 
% cfg.maxConfRT           = Inf;            %secs
% cfg.VAS.bandHeight      = 20;
% cfg.VAS.stringLeft      = 'Sehr unsicher';
% cfg.VAS.stringRight     = 'Sehr sicher';
% cfg.VAS.questionString  = 'Wie sicher?';
% cfg.VAS.nBands          = 10;
% cfg.VAS.bandWidth       = round(dev.xCenter/7);     %width of each colour band in the confidence scale
% cfg.VAS.colorLight      = round(1/2 * 255 * [1 1 1]);
% cfg.VAS.colorDark       = 255 * [1 1 1];
%
% Data are stored in the cell data (global variable) as data{block}{trial}.
% This is necessary in my experiment but likely a bad idea in yours.
% Consider changing it to something easier to navigate.

global cfg;
global dev;
global data;

%draw display intially to get VASrange
VASrange = drawVAS;

%VASrange  = [dev.xCenter-100, dev.xCenter+100];
initialx  = VASrange(1) + (VASrange(2) - VASrange(1)) * rand;
initialx  = round(initialx);
constanty = 0; %We hide the actual cursor. Only the '|' sign on the scale is shown

SetMouse(initialx, 0, dev.wnd, dev.mouseId);
data{block}{trial}.VAS.mousePosOrig = (initialx - VASrange(1))/range(VASrange);
data{block}{trial}.VAS.onset = GetSecs;


%% Initialize

counter       = 1;
mousetrail_x  = [];
movementBegun = 0;
mouseButtons  = 0;
FlushEvents;
correctionPixelsX = 0; % set to 0 to test, check where is that

startVAS = GetSecs;

while (GetSecs - startVAS) < cfg.maxConfRT && ~mouseButtons(1)
    
    [mouseX, mouseY, mouseButtons] = GetMouse;
    if any(mouseButtons(2:length(mouseButtons)))        %TODO looks wrong
        data{block}{trial}.VAS.confidence      = (mouseX -VASrange(1))/range(VASrange); %TODO check that this is correct
        data{block}{trial}.VAS.conf_absolute   = mouseX;
        data{block}{trial}.VAS.movRT           = GetSecs - startVAS;
        break
    end
    
    %restrict the bounds
    if mouseX > VASrange(2)
        mouseX = VASrange(2);
    elseif mouseX < VASrange(1)
        mouseX = VASrange(1);
    end
    
    %update display
    VASrange = drawVAS;
    
    %draw the cursor centered on the mouse x position
    Screen('DrawLine', dev.wnd, 255 * [1 0 0], mouseX, dev.yCenter - cfg.VAS.bandHeight, mouseX, dev.yCenter + cfg.VAS.bandHeight, 4);

    drawShape( struct( 'texture', reft, ...
            'pos', [-0.4 -0.1], ...
            'size', [0.66 0.5]), dev.wnd ); %% size of pictures?, ask Elisa!
        
    drawShape( struct( 'texture', ratet, ...
            'pos', [0.4 -0.1], ...
            'size', [0.66 0.5]), dev.wnd );
    
   
   tFlip = Screen('Flip',dev.wnd);
    
    if mouseX ~= initialx && ~movementBegun
        data{block}{trial}.VAS.firstRT = GetSecs - startVAS;
        movementBegun = 1;
    end
    
    mousetrail_x(counter)   = mouseX - correctionPixelsX;
    moveTime(counter)       = GetSecs - startVAS;
    counter                 = counter + 1;
end


if mouseButtons(1)
    %internal to external extremes
    data{block}{trial}.VAS.confidence      = (mouseX - correctionPixelsX - VASrange(1))/range(VASrange);
    data{block}{trial}.VAS.conf_absolute   = mouseX - correctionPixelsX;
    data{block}{trial}.VAS.movRT           = GetSecs - startVAS;
else
    data{block}{trial}.VAS.confidence    = NaN;
    data{block}{trial}.VAS.conf_absolute = NaN;
    data{block}{trial}.VAS.movRT         = NaN;
end

if ~movementBegun
    data{block}{trial}.VAS.firstRT      = NaN;
end

data{block}{trial}.VAS.mousetrail_x = mousetrail_x;
data{block}{trial}.VAS.moveTime     = moveTime;
Screen('Flip', dev.wnd);
WaitSecs(.3); %this prevents double clicks from messing it all up

end


function VASrange = drawVAS

global dev;
global cfg;


%Bands should be 4 x n matrices with the rows corresponding to: left
%edge, top edge, right edge, bottom edge.
leftEdge  = dev.xCenter + ( (0:(cfg.VAS.nBands -1)) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandWidth);
rightEdge = dev.xCenter + ( (1:(cfg.VAS.nBands   )) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandWidth);
topEdge   = repmat(dev.yCenter - cfg.VAS.bandHeight/2, 1, cfg.VAS.nBands);
lowEdge   = repmat(dev.yCenter + cfg.VAS.bandHeight/2, 1, cfg.VAS.nBands);

bands  = [leftEdge; topEdge; rightEdge; lowEdge];
colors = repmat([cfg.VAS.colorDark' cfg.VAS.colorLight'], 1, cfg.VAS.nBands/2);

VASrange = [min(leftEdge) max(rightEdge)];

Screen('TextSize', dev.wnd, 24);
DrawFormattedText(dev.wnd, cfg.VAS.questionString, 'center', dev.yCenter - 60, cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringRight);
DrawFormattedText(dev.wnd, cfg.VAS.stringRight, VASrange(2) - (TextWidth(3)/2), dev.yCenter + 40, cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringLeft);
DrawFormattedText(dev.wnd, cfg.VAS.stringLeft,  VASrange(1) - (TextWidth(3)/2), dev.yCenter + 40, cfg.txtCol);
Screen('FillRect', dev.wnd, colors, bands);
end


