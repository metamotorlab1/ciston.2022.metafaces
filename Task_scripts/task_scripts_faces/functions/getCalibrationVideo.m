if cfg.timingTest;     
        %Turn on, make sure you see it on video
        writeDigitalPin(dev.arduinoHandle, 'D13', 1);
    end
    
    % Select a moviename for the recorded movie file:
    cfg.fullMovieName = sprintf('SetNewMoviename=%s_calibration.mov', cfg.calibrateMovieName);
    Screen('SetVideoCaptureParameter', dev.vcDevice, cfg.fullMovieName );
    Screen('StartVideoCapture', dev.vcDevice, realmax, 1)
    
    oldtex  = 0;
    tex     = 0;
    RestrictKeysForKbCheck([cfg.escapeChar])
    HideCursor;
    
    while ~KbCheck      % Run until Escape keypress
        
        tex = Screen('GetCapturedImage', dev.wnd, dev.vcDevice, cfg.waitforimage, oldtex);
        if tex > 0                                  % If a texture is available, draw and show it.
            
            %Minimal code
            % Screen('DrawTexture', dev.wnd, tex);    % Draw new texture from framegrabber
            % oldtex = tex;                           % Recycle this texture - faster
            % Screen('Flip', dev.wnd);                % Show it
            
            %This version uses the much more complex OpenGL to flip the
            %image vertically and present it mirrored, which is what we
            %actually want
            Screen('glPushMatrix', dev.wnd);
            Screen('glTranslate', dev.wnd, dev.rect(3)/2, dev.rect(4)/2,0);
            Screen('glScale', dev.wnd, 1,1, 1);        % To zoom in use e.g. Screen('glScale', dev.wnd, -2,2, 1); -1 for mirrored, Carina
            Screen('glTranslate', dev.wnd, -dev.rect(3)/2, -dev.rect(4)/2,0);
            Screen('DrawTexture', dev.wnd, tex );       % Draw new texture from framegrabber
            Screen('glPopMatrix', dev.wnd);
%             DrawFormattedText(dev.wnd, 'Positioniere dich so, dass du dein Gesicht gut sehen kannst.', 'center', 'center', [255 1 1], [], [1]);
%             DrawFormattedText(dev.wnd, 'Druecke Esc wenn du zufrieden bist.', 'center', dev.yCenter + 100, [255 1 1], [], [1]); % text is mirrored
            Screen('Flip', dev.wnd );

            Screen('Close' , tex);
            oldtex = tex;                           % Recycle this texture - faster
        else
            WaitSecs('YieldSecs', 0.005);
        end
    end
    WaitSecs(2); % take longer Video to see change in facial expression?
    Screen('StopVideoCapture', dev.vcDevice);   % Stop capture engine and recording
    RestrictKeysForKbCheck([]);
    
    if cfg.timingTest; 
        %Turn off
        writeDigitalPin(dev.arduinoHandle, 'D13', 0);
    end 
    