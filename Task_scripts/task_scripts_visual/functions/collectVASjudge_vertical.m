
function [data] = collectVASjudge_vertical(feedback, correct)

% 'Plugin' function to display and record responses to a visual analog
% scale.
%
% You'll have to set some configuration parameters in the function calling
% this one. They are:
%
% Get mouseId
% dev.mouseId = GetMouseIndices;
% dev.mouseId = dev.mouseId(1);
%
% [dev.wnd, dev.screenRect] = Screen('OpenWindow', screen, ... your OpenScreen
% command
%
% cfg.txtCol  = 255 * [1 1 1];
% cfg.cursorCol = 255 * [0 0 0];
% dev.xCenter = (dev.screenRect(3) - dev.screenRect(1))/2 + dev.screenRect(1);
% dev.yCenter = (dev.screenRect(4) - dev.screenRect(2))/2 + dev.screenRect(2);
%
%
% cfg.maxConfRT          = Inf;            %secs
% cfg.VAS.bandWidth      = 20;
% cfg.VAS.stringLow      = 'Sehr unsicher';
% cfg.VAS.stringHigh     = 'Sehr sicher';
% cfg.VAS.questionString = 'Wie sicher?';
% cfg.VAS.nBands         = 10;
% cfg.VAS.bandLength     = round(dev.xCenter/7);     %width of each colour band in the confidence scale
% cfg.VAS.colorLight     = round(1/2 * 255 * [1 1 1]);
% cfg.VAS.colorDark      = 255 * [1 1 1];
%
%
% Data is returned in the cell data with the following fields:
% data.VAS.firstRT
% data.VAS.confidence
% data.VAS.conf_absolute
% data.VAS.movRT
% data.VAS.mousetrail_x
% data.VAS.moveTime
% data.VAS.mousePosOrig
% data.VAS.onset
%
% The parameter feedback should be True if feedback should be given and
% False otherwise. The parameter correct is only needed when feedback is
% True, and says if the given answer was correct.

if ~exist('feedback','var')
  feedback=0;
end

if ~exist('correct','var')
  correct=0;
end

global cfg;
global dev;

oldTextSize  = Screen('TextSize', dev.wnd);
oldTextColor = Screen('TextColor', dev.wnd);

VASrange = drawVAS();

%VASrange  = [dev.xCenter-100, dev.xCenter+100];
initialY  = round(rand * (VASrange(2) - VASrange(1)) + VASrange(1));   %start at random position
initialY  = round(initialY);
hiddenx   = 0;

SetMouse(hiddenx, initialY, dev.wnd, dev.mouseId);

data.VAS.mousePosOrig = (initialY - VASrange(1))/range(VASrange);
data.VAS.onset = GetSecs;


%% Initialize

counter       = 1;
mousetrail_y  = [];
movementBegun = 0;
mouseButtons  = 0;
FlushEvents;

startVAS = GetSecs;

while (GetSecs - startVAS) < cfg.maxConfRT && ~mouseButtons(1)
    
    RestrictKeysForKbCheck([KbName('space') cfg.quitkey]);
    [keyIsDown, ~, keyCode ] = KbCheck;
    if keyIsDown && keyCode(KbName('space'))
        break
    elseif keyIsDown && keyCode(cfg.quitkey)
        cfg.experiment_aborted = 1;
        break
    end
    
    [~, mouseY, mouseButtons] = GetMouse;
    %if any(mouseButtons(2:length(mouseButtons)))                            % This allows the subject to right-click to skip the trial
    %    break;
    %end
    
    %restrict the bounds
    if mouseY > VASrange(2)
        mouseY = VASrange(2);
    elseif mouseY < VASrange(1)
        mouseY = VASrange(1);
    end
    
    %update display
    VASrange = drawVAS();
    
    %draw the cursor centered on the mouse y-position
    Screen('DrawLine', dev.wnd, cfg.cursorCol, dev.xCenter - cfg.VAS.bandWidth, mouseY, dev.xCenter + cfg.VAS.bandWidth, mouseY, 4);
    Screen('Flip',dev.wnd);
    
    if mouseY ~= initialY && ~movementBegun
        data.VAS.firstRT = GetSecs - startVAS;
        movementBegun = 1;
    end
    
    mousetrail_y(counter)   = mouseY;
    moveTime(counter)       = GetSecs - startVAS;
    counter                 = counter + 1;
end

if mouseButtons(1)
    data.VAS.confidence      = (VASrange(2) - mouseY)/range(VASrange);
    data.VAS.conf_absolute   = mouseY;
    data.VAS.movRT           = GetSecs - startVAS;
    
    %---------------
    % Give feedback:
    %---------------
        
    if feedback
        if correct
            feedbackColor = 255 * [0 1 0];  %green
        else
            feedbackColor = 255 * [1 0 0];  %red
        end
            
        drawVAS;
        
        %draw the cursor centered on the mouse y-position
        Screen('DrawLine', dev.wnd, feedbackColor, dev.xCenter - cfg.VAS.bandWidth, mouseY, dev.xCenter + cfg.VAS.bandWidth, mouseY, 4);
        Screen('Flip',dev.wnd);
        
        WaitSecs(.5);
    end
    
    
else
    data.VAS.confidence    = NaN;
    data.VAS.conf_absolute = NaN;
    data.VAS.movRT         = NaN;
end

if ~movementBegun
    data.VAS.firstRT      = NaN;
end

data.VAS.mousetrail_y = mousetrail_y;
data.VAS.moveTime     = moveTime;
Screen('Flip', dev.wnd);
WaitSecs(.3); %this prevents double clicks from messing it all up

Screen('TextColor',dev.wnd, oldTextColor); %set back to old color and size
Screen('TextSize', dev.wnd, oldTextSize);

end


function VASrange = drawVAS()

global dev;
global cfg;

%Bands should be 4 x n matrices with the rows corresponding to: left
%edge, top edge, right edge, bottom edge.
topEdge   = dev.yCenter + ( (0:(cfg.VAS.nBands -1)) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandLength);
lowEdge   = dev.yCenter + ( (1:(cfg.VAS.nBands   )) - (cfg.VAS.nBands/2) ) * (cfg.VAS.bandLength);
leftEdge  = repmat(dev.xCenter - cfg.VAS.bandWidth/2, 1, cfg.VAS.nBands);
rightEdge = repmat(dev.xCenter + cfg.VAS.bandWidth/2, 1, cfg.VAS.nBands);

bands  = [leftEdge; topEdge; rightEdge; lowEdge];
colors = repmat([cfg.VAS.colorDark' cfg.VAS.colorLight'], 1, cfg.VAS.nBands/2);

VASrange = [min(topEdge) max(lowEdge)];

Screen('TextSize', dev.wnd, 24);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.questionString);
DrawFormattedText(dev.wnd, cfg.VAS.questionString, dev.xCenter - (TextWidth(3)/2), VASrange(1) - 80, cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringHigh);
DrawFormattedText(dev.wnd, cfg.VAS.stringHigh, dev.xCenter - (TextWidth(3)/2), VASrange(1) - TextWidth(4), cfg.txtCol);
TextWidth = Screen('TextBounds', dev.wnd, cfg.VAS.stringLow);
DrawFormattedText(dev.wnd, cfg.VAS.stringLow,  dev.xCenter - (TextWidth(3)/2), VASrange(2) + TextWidth(4), cfg.txtCol);
Screen('FillRect', dev.wnd, colors, bands);

end