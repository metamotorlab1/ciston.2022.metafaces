function [Info] = run_visual_experiment(varargin)

clear all

global cfg
global dev

realExperiment  = 1;                                                        %set to 1 to hide cursor, etc

% we need these to get size of stimuli in visual angles
% measure these two by hand in every new screen
dev.widthOfScreen_inCm      = 37.478; % testing room: 37.478, office: 59.674
dev.heightOfScreen_inCm     = 29.983; % testing room: 29.983, office: 33.566

if ~realExperiment
    %PsychDebugWindowConfiguration;
    Screen('Preference', 'SkipSyncTests',1);
    dev.widthOfScreen_inCm      = 61;
    dev.heightOfScreen_inCm     = 36;
else
    Screen('Preference', 'SkipSyncTests',0);
end

user = 'MAC'     

switch user
    case 'LINUX'
           myResultPath = '/scratch/share/Dropbox/Data/Faces_task/visual_dots/';
           myPath = '/scratch/share/Dropbox/HiWis/Carina/faces_experiment/faces_task_scripts/'; %pwd; 
    case 'MAC'
         myResultPath = '/Users/metamotorlab/Dropbox/Data/Faces_task/visual_dots/';
         myPath = '/Users/metamotorlab/Dropbox/HiWis/Carina/faces_experiment/faces_task_scripts/'; %pwd;
end

addpath(genpath(myPath))
cd(myPath);

startExperimentTime = tic;

dlg = inputdlg({'ID','Age','Sex','Train Staircase (yes=1, *no*=0)','Audio','Vision','Feedback (yes=1, *no*=0)', '*Buzz* (1) or Beep (2)','Language (*German*=g, English=e)'}, 'Input');
Info.name           = dlg{1};
Info.age            = str2double(dlg{2});
Info.sex            = dlg{3};
cfg.trainstair      = str2double(dlg{4}); % decide if you run a first training staircase (1) or the real expe (0)
cfg.modality.audio  = str2double(dlg{5}); % If you want unimodal, set vision or audio to 2. If you want multimodal, set both to 1.
cfg.modality.vision = str2double(dlg{6});
cfg.feedback        = str2double(dlg{7}); % give feedback on VAS or not
cfg.buzzOrBeep      = str2double(dlg{8}); %1 for buzz; 2 for beep (it's actually coded as if==1; else)
cfg.language        = dlg{9};


if isempty(Info.name),Info.name='test';end
if isnan(Info.age),Info.age=99;end
if isempty(Info.sex),Info.sex='?';end
if isnan(cfg.modality.audio),cfg.modality.audio=0;end
if isnan(cfg.modality.vision),cfg.modality.vision=1;end
if isnan(cfg.trainstair),cfg.trainstair=0;end
if isnan(cfg.feedback),cfg.feedback=0;end
if isnan(cfg.buzzOrBeep),cfg.buzzOrBeep=1;end
if isempty(cfg.language),cfg.language='g';end


%% In all conditions, retrieve the balance for both buzz and beep

balanceAudio_buzzFile = [myResultPath filesep Info.name filesep 'subj' Info.name '_balanceAudio_buzzOrBeep1'];
balanceAudio_beepFile = [myResultPath filesep Info.name filesep 'subj' Info.name '_balanceAudio_buzzOrBeep2'];

if exist([balanceAudio_buzzFile filesep 'alldat_subj' Info.name '_balance.mat'], 'file')
    fromBalance = load([balanceAudio_buzzFile filesep 'alldat_subj' Info.name '_balance.mat']);
    cfg.balanceBuzz = fromBalance.Resu_balanceAudio;
else
    cfg.balanceBuzz = [.5 .5];
end

if exist([balanceAudio_beepFile filesep 'alldat_subj' Info.name '_balance.mat'], 'file')
    fromBalance = load([balanceAudio_beepFile filesep 'alldat_subj' Info.name '_balance.mat']);
    cfg.balanceBeep = fromBalance.Resu_balanceAudio;
else
    cfg.balanceBeep = [.5 .5];
end

keep cfg dev Info realExperiment myPath startExperimentTime myResultPath

%%

%%% in the bimodal condition, retrieve previous unimodal thresholds
%%% (do this now to avoid messing things up by loading old data)
if ~cfg.trainstair
    
    trial2avg=25; % base the threshold on the last trialtomean trials
    fact2mult= 1; % multiplies the training threshold by fact2mult to produce the actual threshold
    threshaud=0;threshvis=0;
    if cfg.modality.vision
        train_vis=[myResultPath filesep Info.name filesep 'subj' Info.name '_audio0_visual' num2str(cfg.modality.vision) '_staircase1_train0'];
        if exist([train_vis filesep 'trialdat_' Info.name '_block1.mat'], 'file')
            fromVision=load([train_vis filesep 'trialdat_' Info.name '_block1.mat']);
            tmpvis=zeros(size(fromVision.Info.T,2),1);
            for t=1:size(tmpvis)
                tmpvis(t)=fromVision.Info.T(t).vision.signalDiff;
            end
            threshvis=mean(tmpvis(length(tmpvis)-trial2avg:length(tmpvis)))*fact2mult; % change to min(.7,...)? before it was 0.45
            fprintf('Previous visual threshold is: %.3f \n',threshvis);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while isempty(threshvalidate)
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
            while ~strcmp(threshvalidate,'y')
                threshvis=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshvis);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
 
        else
            threshvis=input('Can''t find previous unimodal visual threshold, input one (e.g. 0.45): ');
            fprintf('You entered as threshold: %.3f\n',threshvis);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while ~strcmp(threshvalidate,'y')
                threshvis=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshvis);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');            
            end
        end
    end
    
    if cfg.modality.audio
        train_aud=[myResultPath filesep Info.name filesep 'subj' Info.name '_audio' num2str(cfg.modality.audio) '_visual0_staircase1_train0'];
        if exist([train_aud filesep 'trialdat_' Info.name '_block1.mat'], 'file')
            fromAudio=load([train_aud filesep 'trialdat_' Info.name '_block1.mat']);
            tmpaud=zeros(size(fromAudio.Info.T,2),1);
            for t=1:size(tmpaud)
                tmpaud(t)=fromAudio.Info.T(t).audio.signalDiff;
            end
            threshaud=min(4,mean(tmpaud(length(tmpaud)-trial2avg:length(tmpaud)))*fact2mult);
            fprintf('Previous audio threshold is: %.3f \n',threshaud);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while isempty(threshvalidate)
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
            while ~strcmp(threshvalidate,'y')
                threshaud=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshaud);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            end
        else
            threshaud=input('Can''t find previous unimodal audio threshold, input one (e.g. 4): ');
            fprintf('You entered as threshold: %.3f\n',threshaud);
            threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');
            while ~strcmp(threshvalidate,'y')
                threshaud=str2double(threshvalidate);
                fprintf('You entered as threshold: %.3f\n',threshaud);
                threshvalidate=input('Validate by pressing y, or enter a new threshold: ','s');            
            end
        end
    end
    
    keep Info cfg dev threshvis threshaud realExperiment myPath startExperimentTime myResultPath

end


%this will define which stimulus the staircase is done on.
%If both are true, then the staircase will be selected
%randomly, and there will be one (or two) per modality.
if cfg.modality.vision && cfg.modality.audio
    cfg.focus.audio       = 1;
    cfg.focus.vision      = 1;
elseif cfg.modality.audio
    cfg.focus.audio       = 1;
    cfg.focus.vision      = 0;
elseif cfg.modality.vision
    cfg.focus.audio       = 0;
    cfg.focus.vision      = 1;
else
    disp('no valid modalities')
end

commandwindow;                                                              % go to commandwindow to prevent writing on the script
% -----------------------
% Parameters Modifiables:
% -----------------------

%saveDir         = [myPath filesep 'data' filesep 'subj' Info.name '_audio' num2str(cfg.modality.audio) '_visual' num2str(cfg.modality.vision) '_staircase' num2str(cfg.trainstair) '_train' num2str(cfg.feedback)];
saveDir         = [myResultPath filesep Info.name filesep 'subj' Info.name '_audio' num2str(cfg.modality.audio) '_visual' num2str(cfg.modality.vision) '_staircase' num2str(cfg.trainstair) '_train' num2str(cfg.feedback)];

if ~isdir(saveDir), mkdir(saveDir),end

cfg.audio.percentDiff       = [.1 4];                                           % minimal / maximal percent difference tested
if cfg.trainstair && cfg.modality.audio == 1
    cfg.audio.initialDiff(1)  = 2;
    cfg.audio.initialDiff(2)  = .1;
else
    cfg.audio.initialDiff(1)  = 4;
    cfg.audio.initialDiff(2)  = .1;
end
cfg.audio.stepsize          = 0.1;
cfg.audio.levels            = (cfg.audio.percentDiff(1) : cfg.audio.stepsize : cfg.audio.percentDiff(2));


cfg.vision.percentDiff      = [.02 4];

%if P.trainstair && P.modality.vision == 1
%    P.vision.initialDiff(1) = .4;
%    P.vision.initialDiff(2) = .05;
%else
%    P.vision.initialDiff(1) = .55;
%    P.vision.initialDiff(2) = .05;
%end

cfg.vision.initialDiff(1)   = .4;
cfg.vision.initialDiff(2)   = .02;

cfg.vision.stepsize         = .02;                                              % reference is 50 dots, so 1 dot is 0.02
cfg.vision.levels           = (cfg.vision.percentDiff(1) : cfg.vision.stepsize : cfg.vision.percentDiff(2));

cfg.stim.duration           = 0.2;                                             % duration of stimuli (seconds)
cfg.stim.audio.freqs        = [200 1100];                                       % tone frequencies [440 2050]

cfg.stim.audio.rampDur      = .001;                                             % duration of the ramp gradually turning the sound on and off
cfg.stim.audio.sampRate     = 44100;                                            % Sampling Rate Hz
cfg.stim.audio.nTimeSamples = cfg.stim.duration*cfg.stim.audio.sampRate;        % number of time samples TOJ sound

cfg.directions              = [1 2];                                            % possible directions, number of conditions for SOA @ threshold
cfg.congruency              = [0 1];                                            % incongruent or congruent trials

%cfg.trialsPerCond*2 = real length of experiment
switch cfg.trainstair
    case 0
        cfg.trialsPerCond         = 100;  %200 %keep this multiple of 4       % N trials in each strongDirection condition (L-R / R-L)
        cfg.NBlocks               = 4;                                        % Expe divided in NBlocks blocks
    case 1
        cfg.trialsPerCond         = 40;  %keep this multiple of 4             % N trials in each strongDirection condition (L-R / R-L)
        cfg.NBlocks               = 1;                                        % Expe divided in NBlocks blocks
end

cfg.trialsPerBlock        = 2*cfg.trialsPerCond/cfg.NBlocks;
cfg.nStairs               = 2;                                                %2 or 1

% ----------------------
% set general visual parameters
% ----------------------

cfg.viewDist = 55;

bg           = 0.65;
dev.grey     = round(255*[bg bg bg]);


% -----------
% PTB screen:
% -----------
%dev.screenNumber        = max(Screen('Screens'));
dev.screenNumber        = min(Screen('Screens'));

if realExperiment
    [dev.wnd, dev.screenRect]   = Screen('OpenWindow', dev.screenNumber, dev.grey, [], 32,2); %complete screen
else
    [dev.wnd, dev.screenRect]   = Screen('OpenWindow', dev.screenNumber, dev.grey, [0 0 1280 1024], 32,2); %small screen
end    

dev.refreshrate_in_Hz         = Screen('FrameRate', dev.screenNumber);
dev.refreshtime_in_ms         = 1000/dev.refreshrate_in_Hz;
Screen('TextSize',dev.wnd,18);

dev.yCenter         = (dev.screenRect(4) - dev.screenRect(2))/2 + dev.screenRect(2);
dev.xCenter         = (dev.screenRect(3) - dev.screenRect(1))/2 + dev.screenRect(1);


if realExperiment
    HideCursor; % Hide the mouse cursor
end

% -------------
% VAS settings:
% -------------

VAS_Visang              = 20;                                              
ScreenWidthDegrees      = visang(cfg.viewDist, [], dev.widthOfScreen_inCm);
pixPerDeg               = (dev.screenRect(3) - dev.screenRect(1))/ScreenWidthDegrees;
VAS_size                = VAS_Visang * pixPerDeg; 

cfg.txtCol              = 255 * [0 0 0];
cfg.cursorCol           = 255 * [0 0 0];
cfg.maxConfRT           = Inf;            %secs
if cfg.language=='e'
    stringUnsure           = 'Very unsure';
    stringSure             = 'Very sure';
    cfg.VAS.questionString = 'How confident are you?';
elseif cfg.language=='g'
    stringUnsure           = 'Sehr unsicher';
    stringSure             = 'Sehr sicher';
    cfg.VAS.questionString = 'Wie sicher sind Sie?';
end
cfg.VAS.nBands          = 10;
if cfg.modality.vision==1 && cfg.modality.audio==0
    cfg.VAS.bandWidth   = 20;
    cfg.VAS.bandLength  = round(dev.yCenter/10);
    cfg.VAS.stringLow   = stringUnsure;
    cfg.VAS.stringHigh  = stringSure;
else
    cfg.VAS.bandHeight  = 20;
    cfg.VAS.bandWidth   = VAS_size/cfg.VAS.nBands;%round(dev.xCenter/14);     %width of each colour band in the confidence scale
    cfg.VAS.stringLeft  = stringUnsure;
    cfg.VAS.stringRight = stringSure;
end
cfg.VAS.colorLight      = round(1/2 * 255 * [1 1 1]);
cfg.VAS.colorDark       = 255 * [1 1 1];

    
% -----
% Keys:
% -----

KbName('UnifyKeyNames');
cfg.quitkey = KbName('ESCAPE');

cfg.leftAnswerKey     = KbName('LeftArrow');
cfg.rightAnswerKey    = KbName('RightArrow');
cfg.congruentKey      = KbName('W');
cfg.incongruentKey    = KbName('S');
cfg.Question          = '';

if (cfg.modality.audio==1 && cfg.modality.vision==0) || (cfg.modality.audio==0 && cfg.modality.vision==1)
   cfg.signResponseRight = '<--';
   cfg.signResponseLeft  = '-->';
   if cfg.language=='e'
       cfg.legendRight       = 'Left';
       cfg.legendLeft        = 'Right';
   elseif cfg.language=='g'
       cfg.legendRight       = 'Links';
       cfg.legendLeft        = 'Rechts';
   end
else
  cfg.signResponseRight = 'W';
  cfg.signResponseLeft  = 'S';
  if cfg.language=='e'
      cfg.legendRight       = 'Same';
      cfg.legendLeft        = 'Different';
  elseif cfg.language=='g'
      cfg.legendRight       = 'Gleich';
      cfg.legendLeft        = 'Unterschiedlich';
  end
end

%-------
% Mouse:
%-------

%This we'll have to change a bit
mouseId = GetMouseIndices;
mouseId = mouseId(1);

dev.mouseId = mouseId;

%----------------------------
% Visual stimulus parameters
%----------------------------

setDotParams();

%-----------------------------------------------------------
% Generate Sounds for judgement, one sin wave, one sawtooth:
%-----------------------------------------------------------

Sc =  sawtooth(2*pi*cfg.stim.audio.freqs(1)*(0:cfg.stim.duration*cfg.stim.audio.sampRate)/cfg.stim.audio.sampRate);

%Generate baseline sound waves to be modulated in each trial according to a staircase
%pitch 1
Sound_Left{1}  = [cfg.balanceBuzz(1)*Sc;zeros(1,length(Sc))];
Sound_Right{1} = [zeros(1, length(Sc));cfg.balanceBuzz(2)*Sc];

Sc = MakeBeep(cfg.stim.audio.freqs(2),cfg.stim.duration,[cfg.stim.audio.sampRate]);

%Generate baseline sound waves to be modulated in each trial according to a staircase
%pitch 2
Sound_Left{2}  = [cfg.balanceBeep(1)*Sc;zeros(1,length(Sc))];
Sound_Right{2} = [zeros(1, length(Sc));cfg.balanceBeep(2)*Sc];


% ----------------------------------------
% Randomization of the trial presentation:
% ----------------------------------------
% Define trials


% -----------
% Staircase:
% -----------

% All staircases initialized in all cases, not always used

% for test condition, use previous training thresholds
%P.audio.initialDiff=2;P.vision.initialDiff=0.3;
switch cfg.trainstair
    case 0
        for i=1:cfg.nStairs       % this floor()... calculation makes sure to start with a difference of dots that's a multiple of the step size
            Info.staircaseAudio(i) = SetupStaircase(1, floor(threshaud/cfg.vision.stepsize)*cfg.vision.stepsize, [min(cfg.audio.percentDiff) max(cfg.audio.percentDiff)], [2 1]);
            Info.staircaseVision(i) = SetupStaircase(1, floor(threshvis/cfg.vision.stepsize)*cfg.vision.stepsize, [min(cfg.vision.percentDiff) max(cfg.vision.percentDiff)], [2 1]);
        end
    case 1
        for i=1:cfg.nStairs
            Info.staircaseAudio(i) = SetupStaircase(1, cfg.audio.initialDiff(i), [min(cfg.audio.percentDiff) max(cfg.audio.percentDiff)], [2 1]);
            Info.staircaseVision(i) = SetupStaircase(1, cfg.vision.initialDiff(i), [min(cfg.vision.percentDiff) max(cfg.vision.percentDiff)], [2 1]);
        end
end

%-------------------------------------------------------------
% PSEUDORANDOMIZED ORDER OF STIMS & STAIRCASES:
%-------------------------------------------------------------

[congruency]=randConstrNathan(cfg.trialsPerCond, cfg.congruency, 5);

%%% Make sure we have a balanced design between congruency and strongDirection
[strongDirection1]=randConstrNathan(cfg.trialsPerCond/2,  cfg.directions, 5);
[strongDirection2]=randConstrNathan(cfg.trialsPerCond/2,  cfg.directions, 5);

strongDirection(congruency==0)=strongDirection1;
strongDirection(congruency==1)=strongDirection2;

% ------------------------------------------
% Set up staircase for the modality in focus
% ------------------------------------------
StairChoice = repmat([1 cfg.nStairs ],1,length(strongDirection)/2);          % order of ascending/descending staircases
StairChoice = StairChoice(randperm(length(StairChoice)));

for itrial=1:length(strongDirection)
        
    Info.T(itrial).stair                    = StairChoice(itrial);

    Info.T(itrial).congruency               = congruency(itrial);
    Info.T(itrial).audio.strongDirection    = strongDirection(itrial);     %strongDirection(1) is stim 1
    
    %set the strongDirection specifically for each of the two stimuli
    if congruency(itrial)
        Info.T(itrial).vision.strongDirection  = strongDirection(itrial);
    else
        Info.T(itrial).vision.strongDirection  = 3 - strongDirection(itrial);   %if 1--> 2, if 2--> 1
    end
end

% -------------------------------------------------
% Perform basic initialization of the sound driver:
% -------------------------------------------------

InitializePsychSound(1);
%dev.pamaster = PsychPortAudio('Open', 3, 1+8, 1,44100, 2, [],1);
%dev.pamaster = PsychPortAudio('Open', [], 1+8, 2, 44100, 2);
dev.pamaster = PsychPortAudio('Open', [], 1+8, 1,44100, 2, [],1);  % if the sound is not played on the headphones, try the other line instead

%----------------------
% Prepare Audio device:
%----------------------
PsychPortAudio('Start',dev.pamaster, 0,0,1);
dev.paslave  = PsychPortAudio('OpenSlave', dev.pamaster, 1,2);

%-------------------------------------------------------------
%                  Start  Experiment:
%-------------------------------------------------------------

if cfg.language=='e'
    DrawFormattedText(dev.wnd,'Welcome! \n\n\n Press a key to start.','center','center');
elseif cfg.language=='g'
    DrawFormattedText(dev.wnd,'Wilkommen! \n\n\n Druecken Sie eine Taste, um anzufangen.','center','center');
end
Screen('Flip',dev.wnd);

keyIsDown=0;
while keyIsDown==0
    [keyIsDown,~,~] = KbCheck;
end

for  t=1:length(Info.T)
    RestrictKeysForKbCheck([cfg.quitkey]);
    [keyIsDown, ~, keyCode ] = KbCheck;
    if keyIsDown && keyCode(cfg.quitkey)
        break
    end  
    
    %---------------------------------------------------
    % Presentation of fixation cross for the ITI length:
    %---------------------------------------------------
    DrawFormattedText(dev.wnd,'+','center','center');
    Screen('Flip',dev.wnd);
    
    Info.T(t).ITI = (0.5-rand(1))+1;  % intertrial interval in seconds
    WaitSecs(Info.T(t).ITI);
    
    %----------------------------
    % Get the signal intensity difference from staircase:
    %----------------------------
    
    stair = Info.T(t).stair; % choose up/down staircase
    
    %Get the stimulus contrast for the relevant stimulus / stimuli from the
    %respective staircase(s)
    if cfg.modality.vision && cfg.modality.audio                                % bimodal
        Info.T(t).vision.signalDiff      = Info.staircaseVision(stair).Signal;
        Info.T(t).audio.signalDiff       = Info.staircaseAudio(stair).Signal;
    elseif cfg.focus.vision                                                   % unimodal vision
        Info.T(t).vision.signalDiff  = Info.staircaseVision(stair).Signal;
        Info.T(t).audio.signalDiff   = 0;
    else                                                                    % unimodal audio
        Info.T(t).audio.signalDiff   = Info.staircaseAudio(stair).Signal;
        Info.T(t).vision.signalDiff  = 0;
    end
    
    
    %--------------------------------------
    % Set stimuli with appropriate signal strength:
    %--------------------------------------
    
    if Info.T(t).audio.strongDirection == 1
        toggle(1:2) = [1 0];
    elseif Info.T(t).audio.strongDirection == 2
        toggle(1:2) = [0 1];
    end
    if Info.T(t).congruency
        toggle(3:4) = toggle(1:2);
    else
        toggle(3:4) = 1 - toggle(1:2);
    end
    
    if cfg.modality.audio == 2
        soundLeft_freq1  = Sound_Left{1}  + toggle(1) * Info.T(t).audio.signalDiff * Sound_Left{1}; %strongDirection == 1 means left
        soundRight_freq1 = Sound_Right{1} + toggle(2) * Info.T(t).audio.signalDiff * Sound_Right{1};
        soundLeft_freq2  = Sound_Left{2}  + toggle(3) * Info.T(t).audio.signalDiff * Sound_Left{2};
        soundRight_freq2 = Sound_Right{2} + toggle(4) * Info.T(t).audio.signalDiff * Sound_Right{2};
    elseif cfg.modality.audio == 1
        soundLeft_freq1  = Sound_Left{1}  + toggle(1) * Info.T(t).audio.signalDiff * Sound_Left{1}; %strongDirection == 1 means left
        soundRight_freq1 = Sound_Right{1} + toggle(2) * Info.T(t).audio.signalDiff * Sound_Right{1};
        soundLeft_freq2  = Sound_Left{2}  + toggle(1) * Info.T(t).audio.signalDiff * Sound_Left{2};
        soundRight_freq2 = Sound_Right{2} + toggle(2) * Info.T(t).audio.signalDiff * Sound_Right{2};
    end
    
    if cfg.modality.audio == 2
        totalSound_Left  = (soundLeft_freq1  + soundLeft_freq2)/2;
        totalSound_Right = (soundRight_freq1 + soundRight_freq2)/2;
        totalSound= (totalSound_Left+totalSound_Right)/2;
        
    elseif cfg.modality.audio == 1
        if cfg.buzzOrBeep == 1
            totalSound_Left  = soundLeft_freq1;
            totalSound_Right = soundRight_freq1;
            totalSound= (totalSound_Left+totalSound_Right)/2;
        else
            totalSound_Left  = soundLeft_freq2;
            totalSound_Right = soundRight_freq2;
            totalSound= (totalSound_Left+totalSound_Right)/2;
        end
    end
    
    
    %Visual
    %make gabor ad hoc the quests suggestion
    
    %n = cfg.stim.vision.REF*(1+(2*(rand>.5)-1).*max(0,Info.T(t).vision.signalDiff)); % either greater or less than standard, random  (staircase can go negative but only values >=0 are used, avoids boundary problem)
    n = cfg.stim.vision.REF*(1+Info.T(t).vision.signalDiff);
    n = round(n);
    n = max(1,n);
    n = min(n,2*cfg.stim.vision.REF);    % bound by 1 and 2*REF
    
    n_high = max([n, cfg.stim.vision.REF]);
    n_low  = min([n, cfg.stim.vision.REF]);
    
    if Info.T(t).vision.strongDirection == 1
        toggle(1:2) = [1 0];
    elseif Info.T(t).vision.strongDirection == 2
        toggle(1:2) = [0 1];
    end
    if Info.T(t).congruency
        toggle(3:4) = toggle(1:2);
    else
        toggle(3:4) = 1 - toggle(1:2);
    end
    
    if cfg.modality.vision == 2
        n = toggle * n_high + (1-toggle) * n_low;
    elseif cfg.modality.vision == 1
        n = toggle(1:2) * n_high + (1-toggle(1:2)) * n_low;
    end
    
    %-------------------
    % Display the stimuli
    %-------------------
    
    whentoplay=GetSecs+0.1;
    
    if cfg.modality.vision
        
        Screen('FrameOval',dev.wnd,cfg.stim.vision.dotcolor,cfg.stim.vision.gratingRect(:,1),cfg.stim.vision.pen_width);
        Screen('FrameOval',dev.wnd,cfg.stim.vision.dotcolor,cfg.stim.vision.gratingRect(:,2),cfg.stim.vision.pen_width);
        if cfg.modality.vision==2
            Screen('FrameOval',dev.wnd,cfg.stim.vision.dotcolor,cfg.stim.vision.gratingRect(:,3),cfg.stim.vision.pen_width);
            Screen('FrameOval',dev.wnd,cfg.stim.vision.dotcolor,cfg.stim.vision.gratingRect(:,4),cfg.stim.vision.pen_width);    
        end
        
        for side = 1:size(n,2)
            xy = dotcloud(n(side),cfg.stim.vision.dotsize);
            xy = xy*0.95*cfg.stim.vision.inner_circle*dev.fov;
            xy = xy/2*eye(2);
            Info.T(t).vision.drawnXY{side} = xy;

            z = cfg.stim.vision.dotsize*0.95*cfg.stim.vision.inner_circle*dev.fov;

            for i=1:n(side)
                wh = xy(i,[1 2 1 2])  +  [-z/2 -z/2 z/2 +z/2] + [ cfg.stim.vision.centers(:,side); cfg.stim.vision.centers(:,side) ]';
                Screen('FillOval', dev.wnd, cfg.stim.vision.dotcolor,wh);
            end
        end
    end
    
    %-------------------
    % And/or Play the Sounds:
    %-------------------
    
    if cfg.modality.audio
        PsychPortAudio('FillBuffer', dev.paslave, totalSound);
        Info.T(t).audio.onset = PsychPortAudio('Start', dev.paslave, 1, whentoplay,1);
    end
    
    if cfg.modality.vision
        Info.T(t).vision.onset = Screen('Flip', dev.wnd, whentoplay);
        WaitSecs(cfg.stim.duration);
    end
    
    if cfg.modality.audio
        PsychPortAudio('Stop', dev.paslave,1);
    end
    
    
    %--------------------------------
    %Start checking for the response:
    %--------------------------------
    if ((cfg.modality.audio==1 && cfg.modality.vision==0) || (cfg.modality.audio==0 && cfg.modality.vision==1))
        RestrictKeysForKbCheck([cfg.leftAnswerKey cfg.rightAnswerKey]);
    else
        RestrictKeysForKbCheck([cfg.congruentKey cfg.incongruentKey]);
    end
    DrawFormattedText(dev.wnd, cfg.Question,'center','center',0);
    Screen('TextSize', dev.wnd, 24); DrawFormattedText(dev.wnd, cfg.signResponseRight, 'center' , dev.yCenter- 50,0);
    Screen('TextSize', dev.wnd, 20); DrawFormattedText(dev.wnd, cfg.legendRight, 'center' , dev.yCenter - 80,0);
    Screen('TextSize', dev.wnd, 24); DrawFormattedText(dev.wnd, cfg.signResponseLeft, 'center' , dev.yCenter+ 80,0);
    Screen('TextSize', dev.wnd, 20); DrawFormattedText(dev.wnd, cfg.legendLeft, 'center' , dev.yCenter+50,0);
    Screen('Flip',dev.wnd);
    
    keyIsDown = 0;
    
    while keyIsDown == 0 %if the key is not down, go into the loop
        [keyIsDown, ~, keyCode] = KbCheck; %read the keyboard
    end
    
    switch cfg.modality.vision
        case 0
            Info.T(t).RT = GetSecs-Info.T(t).audio.onset; % ref= audio onset
        otherwise
            Info.T(t).RT = GetSecs-Info.T(t).vision.onset; % ref= visual onset
    end
    
    
    %-----------------------------------------------------
    % Calculate if response was correct or not & feedback:
    %-----------------------------------------------------
    
    if KbCheck == 1
        wasCong = Info.T(t).congruency;     
        
        % in case we do unimodal experiment with only one pair of stimuli
        if cfg.modality.audio == 1 && cfg.modality.vision==0
            if  keyCode(cfg.leftAnswerKey) && Info.T(t).audio.strongDirection==1
                Info.T(t).ResponseCorrect = 1;
            elseif keyCode(cfg.rightAnswerKey) && Info.T(t).audio.strongDirection==2
                Info.T(t).ResponseCorrect = 1;
            else
                Info.T(t).ResponseCorrect = 0;
            end
        elseif cfg.modality.vision == 1 && cfg.modality.audio==0
            if  keyCode(cfg.leftAnswerKey) && Info.T(t).vision.strongDirection==1
                Info.T(t).ResponseCorrect = 1;
            elseif keyCode(cfg.rightAnswerKey) && Info.T(t).vision.strongDirection==2
                Info.T(t).ResponseCorrect = 1;
            else
                Info.T(t).ResponseCorrect = 0;
            end
            
        % for unimodal staircases before unimodal conditions, and main experiment
        else
            if  keyCode(cfg.congruentKey) && wasCong
                Info.T(t).ResponseCorrect = 1;
                
            elseif keyCode(cfg.congruentKey) && ~wasCong
                Info.T(t).ResponseCorrect = 0;
                
            elseif keyCode(cfg.incongruentKey) && ~wasCong
                Info.T(t).ResponseCorrect = 1;
                
            elseif keyCode(cfg.incongruentKey) && wasCong
                Info.T(t).ResponseCorrect = 0;
                
            end
        end
        
    end
    
    % sprintf('Congruency: %d',Info.T(t).congruency)
    % store response in staircase
    Info.staircaseAudio(stair) = StaircaseTrial(1, Info.staircaseAudio(stair), Info.T(t).ResponseCorrect);
    Info.staircaseAudio(stair) = UpdateStaircase(1, Info.staircaseAudio(stair), -cfg.audio.stepsize);         %this -stepsize is confusing, but correct
    
    Info.staircaseVision(stair) = StaircaseTrial(1, Info.staircaseVision(stair), Info.T(t).ResponseCorrect);
    Info.staircaseVision(stair) = UpdateStaircase(1, Info.staircaseVision(stair), -cfg.vision.stepsize);
    
    %-----------------------------
    % Ask for confidence judgment in test condition only
    %-----------------------------
    if ~cfg.trainstair
        %show VAS, wait for answer, etc
        if cfg.modality.vision==1 && cfg.modality.audio==0
            [data] = collectVASjudge_vertical(cfg.feedback, Info.T(t).ResponseCorrect);
        else
            [data] = collectVASjudge(cfg.feedback, Info.T(t).ResponseCorrect);
        end
        Info.T(t).VAS = data.VAS;
        commandwindow;              % go to commandwindow to prevent writing on the script
    end
    
    
    if ~mod(t,cfg.trialsPerBlock)
        Screen('TextSize',dev.wnd,18);
        if t ~= length(Info.T)
            if cfg.language=='e'
                DrawFormattedText(dev.wnd,'You can now take a break. \n\n\n Press C when you are ready to start.','center','center');
            elseif cfg.language=='g'
                DrawFormattedText(dev.wnd,'Sie koennen jetzt eine Pause machen. \n\n\n Druecken Sie C, wenn Sie fortfahren moechten.','center','center');
            end
        else
            if cfg.language=='e'
                DrawFormattedText(dev.wnd,'That was the last one! Press C to end.','center','center');
            elseif cfg.language=='g'
                DrawFormattedText(dev.wnd,'Das war der Letzte! Druecken Sie C, um das Experiment zu beenden.','center','center');
            end
        end
        Screen('Flip',dev.wnd);
        %%%% save relevant infos per trial in case of a crash
        save([saveDir filesep 'trialdat_' Info.name '_block' num2str(ceil(t/cfg.trialsPerBlock)) '.mat']);
        
        RestrictKeysForKbCheck(KbName('C'));
        KbWait;
        %                 WaitSecs(120);
    end   
  
end

WaitSecs(.2);
Screen('CloseAll')
experimentDuration = toc(startExperimentTime);
disp(['This took ' num2str(experimentDuration) ' seconds.'])

keep t dev cfg realExperiment myPath saveDir Info startExperimentTime experimentDuration myResultPath

%    save([saveDir filesep 'TOJ_Audio_' name '_' datestr(now,'dd.mm.yyyy') '.mat'], 'Info', 'P');
%%% save everything at the end in case we forgot something
save([saveDir filesep 'alldat_' Info.name '_' datestr(now,'dd.mm.yyyy') '.mat']);

fprintf('\n-----------------------\n\n');

PsychPortAudio('Close' , dev.pamaster);

% workaround bug plot if script stops before the end
if t~=length(Info.T),t=t-1;end

% plot the staircases
for tr = 1:t
    stair(tr) = Info.T(tr).stair;
end


figure; hold on;
xlabel('trial number'); ylabel('signalDiff (sec)'); title('Check staircases')

if cfg.focus.vision == 1
    for tr = 1:t
        vision.signalDiffs(tr)= Info.T(tr).vision.signalDiff;
    end
    plot(vision.signalDiffs(stair==1), 'bo-');
    plot(vision.signalDiffs(stair==2), 'ro-');
end

if cfg.focus.audio == 1
    for tr = 1:t
        audio.signalDiffs(tr)= Info.T(tr).audio.signalDiff;
    end
    plot(audio.signalDiffs(stair==1), 'b*-');
    plot(audio.signalDiffs(stair==2), 'r*-');
end
savefig([saveDir filesep 'staircase.fig'])


% plot raw confidence histogram
if ~ cfg.trainstair
    conf = zeros(t,1);
    for tr = 1:t
        conf(tr)= Info.T(tr).VAS.confidence;
    end
    figure;
    histogram(conf, 0:0.1:1)
    savefig([saveDir filesep 'districonf.fig'])
end

end