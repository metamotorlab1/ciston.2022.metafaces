%% set up

clear all

dataFolder = '/metamotor/Datasets/Faces_task/test_data/';
outputFolder = '/metamotor/Datasets/Faces_task/output/';
imagesFolder = '/metamotor/Datasets/Faces_task/landmarks_analysis/';
%fxFolder = '/scratch/share/Dropbox/HiWis/Carina/faces_experiment/functions';

%addpath(fxFolder);

showDiagnosticPlots = [0 0];

%% initialize
%Identify subject folders
subjects = dir(dataFolder);
directories = {subjects.name};
idx = find(~cellfun(@(x) isempty(regexpi(x, '^[0-9]', 'once')), directories));
subjDIRs = directories(idx);

fields = {'subject', 'subjID', 'trial', 'block', 'frameNumber', 'distance_RSSQ', ...
    'confidence', 'postHocRating', 'RT_takePic', 'RT_conf', 'expression'};

load('average_face.mat')

%% Add variables for landmarks to fields
% Include absolute landmark locations? Difference? Normalized values? Magnitude & angle?
for lmID = 1:68
    for pos = {'x' 'y'}
        fields = {fields{:} sprintf('lm%.2i%s', lmID, pos{:})};
    end
end
c = nan(1,length(fields));
allSubjs = array2table(c,'VariableNames',fields);
allSubjs(1,:) = [];

%%

for subj =  1:length(subjDIRs)

    subjID = subjDIRs{subj};
    curr_subjDIR = fullfile(dataFolder, subjID);


    %subj 3, many frames, no idea why (maybe double chin?)
    %removed 2nd values for landmarks, old landmarks
    %(landmarks_beforecleanup.mat)
    %subj 12, 1 frame (209)
    %subj 15, 3 frames (MY FACE)
    %subj 16: frame 68 is missing,
    %subj 22, frame 161 (MY FACE)
    %subj 29: frame246 is missing,
    %subj 31, 2 frames (14 and 130)
    %subj 33, frame 0 (MY FACE)
    %subj 37, frame 1 (MY FACE, exclude)
    %subj 36: frame179 is missing,

    %I would suggest to use always the first value for each
    %landmark

    % get the data
    clear data
    possibleFiles = dir(fullfile(dataFolder, subjID));
    files = {possibleFiles.name};
    idx = find(~cellfun(@(x) isempty(regexpi(x, '^[0-9].*\.mat', 'once')), files));
    resultFile = files{idx(1)}; %the second one is longer and includes _training

    load(fullfile(curr_subjDIR,resultFile))

    %reconstruct picturePairs
    for block = 2:size(data,1) % we start on block 2 because block 1 is just the response to the actors' faces
        for trial = 1:size(data{block},1)
            picturePairs(block,trial) = data{block}{trial}.getpicturefromtrial;
        end
    end
    picturePairs(1,:) = NaN;

    %match picturePairs to the frame number (python's landamrk positions
    %are zero-indexed)
    %First line actually only has 32 trials, but marking it as 64 so meet
    %matrix dimensions
    correspondingFrameNumber = [1:32 nan(1,32);
        33:96;
        97:160;
        161:224;
        225:288] -1; %-1 because frame indexing starts at 0


    %get landmark coordinates
    clear landmarks
    load(fullfile(curr_subjDIR, 'landmarks.mat'))

    %and the subsequent side-by-side ratings
    clear ratedata
    load(fullfile(curr_subjDIR, 'rating.mat'))

    % add missing frame as NaN (does not work for procruste function, ask
    % Elisa?

    %landmarks.Frame68 = NaN(1,68,3);

    %landmarks.reshape(1,68,2);

    %set the first block, which is a bit different from the rest
    for b = 1
        trialsFirstBlock = 1:64;
        distance_RSSQ(trialsFirstBlock,b) = NaN;
        RT_conf(trialsFirstBlock,b) = NaN;
        expressionRepetition(trialsFirstBlock,b) = NaN;
        confidence(trialsFirstBlock,b) = NaN;
        for t = 1:32
            RT_takePic = data{b}{t}.responseTime;
            expression = data{b}{t}.facialexpression;
        end
        RT_takePic(33:64,b) = NaN;
        expression(33:64,b) = NaN;
    end

    % Get the distance between two coordinates sets for each picture pair
    for b = 2:size(data,1) % we start on block 2 because block 1 is just the reference faces (response to the actors' faces)
        for t = 1:size(data{b},1)

            trial_table = array2table(c,'VariableNames',fields);

            % Set info variables
            trial_table.subject = subj;
            trial_table.subjID = {subjID};
            trial_table.trial = t + (b-1)*32;
            trial_table.block = b;
            trial_table.frameNumber = correspondingFrameNumber(b,t);
            % Done

            referenceFrame = picturePairs(b,t)-1; %frame indexing starts at 0
            responseFrame  = correspondingFrameNumber(b,t);

            if isfield(landmarks, ['Frame' num2str(responseFrame)])

                if showDiagnosticPlots(1)
                    %load reference image
                    refPic = imread([imagesFolder subjID filesep 'frame' num2str(referenceFrame) '.png']); % '/' etc picturePairs(b,t) '.png'
                    responsePic = imread([imagesFolder subjID filesep 'frame' num2str(responseFrame) '.png']);

                    figure; hold on
                    subplot(121); image(refPic)
                    subplot(122); image(responsePic)
                    commandwindow; input('Pair OK?')
                    close
                end

                %get distances
                ref_xyz = landmarks.(['Frame' num2str(referenceFrame)]);
                response_xyz = landmarks.(['Frame' num2str(responseFrame)]);
                %reshape (ignoring z for now)
                reference_xy = squeeze(ref_xyz(:,:,1:2));
                response_xy = squeeze(response_xyz(:,:,1:2));

                %Align with procrustes

                %1. Either use all the points
                %[alignDist(t,b), aligned_xy] = procrustes(reference_xy, response_xy);
                %2. Or use selected points (eg face outline, dim2 1:17) to align and transform the rest
                %check the landmark indexes
                %scatter(ref_xyz(:,:,1),-ref_xyz(:,:,2)); hold on
                %scatter(ref_xyz(1,1:17,1),-ref_xyz(1,1:17,2), 'r')

                alignmentPoints = [34 37 46]; % tip of nose and outside eyes
                % alignmentPoints = [34 37 40 43 46]; % tip of nose and all eye corners
                %alignmentPoints = [28:37 40 43 46]; % full nose and eye corners
                %alignmentPoints = 1:17;

                %Get transformation parameters for specified invariant
                %points and and transform all the points of the face
                xy_separate = 0;

                if ~xy_separate
                    % Align both axes dependently
                    [alignDist, ~, transf] = procrustes(average_face(alignmentPoints,:), reference_xy(alignmentPoints,:));
                    aligned_ref = transf.b * reference_xy * transf.T + repmat( transf.c(1,:),length(reference_xy), 1);

                    [alignDist, ~, transf] = procrustes(average_face(alignmentPoints,:), response_xy(alignmentPoints,:));
                    aligned_resp = transf.b * response_xy * transf.T + repmat( transf.c(1,:),length(response_xy), 1);

                else
                    % Separate procrustes dimensions (independent alignment)



                end

                diff_xy = aligned_resp - aligned_ref;

                %Plot original and aligned (here's where grouping into the same
                %expression would have worked. will be done as an additional
                %chek in next step, after the b,t loop)
                if showDiagnosticPlots(2)
                    xl = [0, 2000];
                    yl = [-1500, 200];
                    figure;
                    plot(average_face(:,1), -average_face(:,2), 'k-o'); hold on
                    %xlim(xl); ylim(yl);
                    %figure;
                    plot(aligned_ref(:,1), -aligned_ref(:,2), 'b-o'); hold on
                    %xlim(xl); ylim(yl);
                    %figure;
                    plot(aligned_resp(:,1),-aligned_resp(:,2), 'r-o');
                    %xlim(xl); ylim(yl);
                    legend('average','reference','response')
                    %legend('original','procrustes')
                    commandwindow; input('Pair OK?')
                    close
                end

                %Calculate remaining distance
                %we want rssq on the rows (x,y pairs) and then overall, bu the
                %order does not affect the numerical result
                %TODO (elisa): consider the distance only based on face without outline
                trial_table.distance_RSSQ = rssq(rssq(aligned_ref - aligned_resp, 2));

                % Save x,y distances
                axis = {'x' 'y'};
                for pt = 1:length(diff_xy)
                    for ax = 1:length(axis)
                        trial_table.(sprintf('lm%.2i%s', pt, axis{ax})) = diff_xy(pt,ax);
                    end
                end


                %extract other useful stuff
                trial_table.confidence = data{b}{t}.VASrealtask.confidence;
                trial_table.RT_conf    = data{b}{t}.VASrealtask.movRT; %TODO: check what the reference is
                trial_table.RT_takePic = data{b}{t}.responseTime; %CAREFUL! Is this correct? There are several different values here. responseTime, keypress,pictureTime
                trial_table.expression = data{b}{t}.facialexpression;

                %get the rating on the second phase of the experiment (when
                %they saw the picture side-by-side)
                if subj == 29 && b == 5
                    load(fullfile(curr_subjDIR, 'rating_block5.mat'))
                end
                trial_table.postHocRating = ratedata{b}{t}.VASrateimg.confidence;

                % Compile trial info into main table
                allSubjs = [allSubjs; trial_table]; %#ok<AGROW>
            end
        end

    end


%     %bundle everything together
%     allSubjs.subject = [allSubjs.subject; repmat(subj,numel(correspondingFrameNumber),1)];
%     allSubjs.subjID  = [allSubjs.subjID; repmat(subjID,numel(correspondingFrameNumber),1)];
%
%     trialIndex = correspondingFrameNumber' +1;
%     allSubjs.trial = [allSubjs.trial; trialIndex(:)];
%
%     blockIndex = [repmat(1,32,1);
%         nan(32,1);
%         repmat(2,64,1);
%         repmat(3,64,1);
%         repmat(4,64,1);
%         repmat(5,64,1)];   %#ok<REPMAT>
%     allSubjs.block = [allSubjs.block; blockIndex];
%
%     allSubjs.distance_RSSQ = [allSubjs.distance_RSSQ; distance_RSSQ(:)];
%     allSubjs.confidence = [allSubjs.confidence; confidence(:)];
%     allSubjs.RT_takePic = [allSubjs.RT_takePic; RT_takePic(:)];
%     allSubjs.RT_conf = [allSubjs.RT_conf; RT_conf(:)];
%     allSubjs.expression = [allSubjs.expression; expression(:)];
%     allSubjs.frameNumber = [allSubjs.frameNumber; trialIndex(:)-1];
%     allSubjs.postHocRating = [allSubjs.postHocRating; postHocRating(:)];

end

%struct2csv(allSubjs, [outputFolder 'allSubjsDistances.csv'])
%save([outputFolder 'allSubjsDistances.mat'], 'allSubjs')
writetable(allSubjs, 'ML_allSubjsDistances_standardNorm.csv')
