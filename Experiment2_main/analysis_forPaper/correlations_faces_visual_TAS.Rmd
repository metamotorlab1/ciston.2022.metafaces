---
title: "Faces Analysis - Explore relationship to covariates - Experiment 2"
author: "EF"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    toc: true
    toc_float:
      collapsed: false
      smooth_scroll: false
code_folding: show
number_sections: true
---


```{r setup, include=FALSE}
rm(list =ls())
library(tidyverse)
library(ggplot2)
library(here)
library(patchwork)
library(R.matlab)
library(bayestestR)
library(brms)
library(tidybayes) #add_fitted_draws to plot the probit model
library(mixedup)
library(BayesFactor)
source(here::here('functions_rainclouds', "R_rainclouds.R")) #not sure what's masking `here`, but this works
source(here::here('functions_rainclouds','summarySE.R'))
library(ggsci)
library(cowplot) # devtools::install_github("wilkelab/cowplot")
library(magrittr) # for pipes and %<>%
library(ggpubr) # for theme_pubr()



knitr::opts_chunk$set(echo = TRUE)

substrRight <- function(x, n){
  substr(x, nchar(x)-n+1, nchar(x))
}
```


## 1. Are our participants generally bad at metacognition? 
Correlate participant-wise slopes in the confidence_distance model to mratio in visual dots  

### Load model 
```{r load_confidenceVsDistance}
SubjConf_distanceMeasures.full <- brm(file = "brms_models/SubjConf_distanceMeasures_subjID.full")
similarity_distanceMeasures.full <- brm(file = "brms_models/similarity_distanceMeasures_subjID.full")
```

## Extract slopes per subject
Load them if you've ran this before, extracting takes a bit
```{r confidenceVsDistance_getSlopes}

faces.confVsDist <- mixedup::extract_random_coefs(SubjConf_distanceMeasures.full, re = 'subjID') %>% 
  filter(effect == "log_distance_RSSQ") %>% 
  mutate(subjID = group) %>% 
  select(subjID,value) %>% 
  print(n=40)
save(faces.confVsDist,file=here('individualSlopes','faces.confVsDist.Rda'))

load(here('individualSlopes','faces.confVsDist.Rda'))

#Sanity check: I checked that these above do match coef(SubjConf_distanceMeasures.full)$subject[,,2][,1], and both match (roughly, from eyeballing it) the means of the posterior draws in the figures I created in the other ("models") script. The mixedup package has the additional advantage of returning the subject numbers as well. 

faces.similarityVsDist <- mixedup::extract_random_coefs(similarity_distanceMeasures.full, re = 'subjID') %>% 
  filter(effect == "log_distance_RSSQ") %>% 
  mutate(subjID = group) %>% 
  select(subjID,value) %>% 
  print(n=40)
save(faces.similarityVsDist, file = here('individualSlopes','faces.similarityVsDist.Rda'))

load(here('individualSlopes','faces.similarityVsDist.Rda'))



#Sanity check: plot the similarity rating slopes to make sure that the extracted estimates match
similarity_distanceMeasures.full %>%
  tidybayes::spread_draws(b_log_distance_RSSQ, r_subjID[subjID,]) %>%
  mutate(slope = b_log_distance_RSSQ + r_subjID) %>%
  ggplot(aes(x = subjID, y = slope)) +
  stat_slab() +
  labs(y = "Effect of distance", x = "Participant") +
  coord_cartesian(ylim=c(-0.5,0.5))


```

## Load data from visual task  
```{r addVisualCovariates}

data.visual.raw <- readMat(here('visualdots/metavisual_all.mat')) 
SSEfits <- data.visual.raw$allSubjs[,,1]$SSEfit

data.visual <- data.frame(subjID = character(dim(SSEfits)[3]),
                          visual.Mratio = numeric(dim(SSEfits)[3]),
                          stringsAsFactors = FALSE)
for (subj in c(1:7,9:dim(SSEfits)[3])){
  data.visual$visual.Mratio[subj] <- SSEfits[,,subj]$M.ratio
  data.visual$subjID[subj] <- substrRight(data.visual.raw$subjectNames[[subj]][[1]], 6)
  data.visual$subjID[subj] <- substr(data.visual$subjID[subj], 1,5)
}
data.visual$percentCorrect <- data.visual.raw$allSubjs[,,1]$percentCorrect[,1]
data.visual

#get extracted trials
rejectedTrials <- data.matrix(
  data.visual.raw$allSubjs[,,1]$shortRT1count +
data.visual.raw$allSubjs[,,1]$longRT1count +
data.visual.raw$allSubjs[,,1]$shortRT2count +
data.visual.raw$allSubjs[,,1]$longRT2count)

mean(rejectedTrials/200) * 100 #trials/subject * percentage
min(rejectedTrials)
max(rejectedTrials)
```



```{r addTASCovariates}

data.TAS.raw <- readMat(here('TAS/TASscore.mat')) 
data.TAS <- data.frame(summedTASScore = data.TAS.raw$TAS[,,1]$TASscore,
                       subjID = character(dim(SSEfits)[3]),
                       stringsAsFactors = FALSE)

for (subj in c(1:dim(data.TAS)[1])){
  data.TAS$subjID[subj] <- data.TAS.raw$TAS[,,1]$subjIDs[[subj]][[1]]
}

#Fix typo during testing TAS: Subject 155BB in the TAS variable is subject 151BB in Matlab
data.TAS$subjID[data.TAS$subjID=="155BB"] <- "151BB"

#Merge all covariates
covariates <- list(faces.confVsDist, faces.similarityVsDist, data.visual, data.TAS) %>% reduce(left_join, by = "subjID")
covariates <- covariates %>% 
  rename("faces.confVsDist" = "value.x") %>% 
  rename("faces.similarityVsDist" = "value.y") %>% 
  rename("visual.percentCorrect" = "percentCorrect")

write_csv(covariates, here('covariates_faces.csv'))

#Remove a single subject ("110JA") with mratio = 3 who because of dropbox auto synch (and using functions from other folders) ended up using a confidence scale that gave her performance feedback
covariates <-covariates %>% 
  drop_na() 

```


```{r statsVisualMratio}
# Test if  visual Mratio different from 0
mean(covariates$visual.Mratio)
sd(covariates$visual.Mratio)
t.test(covariates$visual.Mratio)
ttestBF(covariates$visual.Mratio)

# Test if correlation Mratio~faces different from 0
Hmisc::rcorr(covariates$faces.confVsDist,covariates$visual.Mratio)
ConfidenceSlopeVsVision <- lm(faces.confVsDist ~ visual.Mratio, data=covariates)
anova(ConfidenceSlopeVsVision)
correlationBF(covariates$faces.confVsDist, covariates$visual.Mratio)

```


```{r plotCorrelationCovars}

confidenceVsVision.plot <- ggplot(covariates, aes(x=faces.confVsDist, y=visual.Mratio)) +
  geom_point() +
  geom_smooth(method = 'lm', formula = y~x, aes(color=pal_lancet("lanonc")(9)[2])) +
  labs(y = "Visual metacognitive efficiency (Mratio)", 
       x = "Random slope of distance on confidence ") +
  theme(legend.position = "none") +
    coord_cartesian(ylim = c(-1,3.5)) +
   ggtitle("A.")

visualMratio.marginal <-ggplot(covariates, aes(x = 0, y = visual.Mratio, fill = "black")) +
  geom_flat_violin(position = position_nudge(x = .1, y = 0), adjust = 1.5, trim = FALSE, alpha = .7, colour = NA)+
  geom_boxplot(aes(x = 0, y = visual.Mratio, fill = "black"), outlier.shape = NA, alpha = .7, width = .1, colour = "black")+
  ggtitle(" ") +
  labs(y = "Mratio") + 
  theme(legend.position = "none", 
        axis.title =element_blank(),
        axis.text =element_blank(),
        axis.ticks =element_blank()) +
  coord_cartesian(ylim = c(-1,3.5)) +
  geom_hline(yintercept = 0, linetype="dashed") +
  scale_x_continuous(breaks = NULL)

Figure4.A <- confidenceVsVision.plot + visualMratio.marginal + plot_layout(widths = c(2, 1))



confidenceVsTAS.plot <- ggplot(covariates, aes(x=faces.confVsDist, y=summedTASScore)) +
  geom_point() +
  geom_smooth(method = 'lm', formula = y~x, aes(color=pal_lancet("lanonc")(9)[2])) +
  labs(y = "Summed TAS Score", x = "Random slope of distance on confidence ") +
  theme(legend.position = "none") +
    coord_cartesian(ylim = c(10,80)) +
   ggtitle("B.")

summedTASScore.marginal <-ggplot(covariates, aes(x = 0, y = summedTASScore, fill = "black")) +
  geom_flat_violin(position = position_nudge(x = .1, y = 0), adjust = 1.5, trim = FALSE, alpha = .7, colour = NA)+
  geom_boxplot(aes(x = 0, y = summedTASScore, fill = "black"), outlier.shape = NA, alpha = .7, width = .1, colour = "black")+
  ggtitle(" ") +
  labs(y = "Summed TAS Score") + 
  theme(legend.position = "none", 
        axis.title =element_blank(),
        axis.text =element_blank(),
        axis.ticks =element_blank()) +
  coord_cartesian(ylim = c(10,80)) +
  scale_x_continuous(breaks = NULL)

Figure4.B <- confidenceVsTAS.plot + summedTASScore.marginal + plot_layout(widths = c(2, 1))
Figure4.B


Figure4 <- confidenceVsVision.plot + visualMratio.marginal + 
  confidenceVsTAS.plot + summedTASScore.marginal + 
  plot_layout(widths = c(4,1,4,1)) +
  plot_annotation(title = 'Participant-wise metacognitive measures')

ggsave(here("../../Figures/Figure4.png"), 
       #device = cairo_pdf, #only if pdf
       plot = Figure4, 
       type = "cairo", 
       dpi = 300,
       width = 210, 
       height = 80, 
       units = "mm")
 

VisionVsTAS.plot <- ggplot(covariates, aes(x=visual.Mratio, y=summedTASScore)) +
  geom_point() +
  geom_smooth(method = 'lm', formula = y~x, aes(color=pal_lancet("lanonc")(9)[2])) +
  labs(y = "Summed TAS Score", x = "Visual metacognitive efficiency (Mratio) ") +
  theme(legend.position = "none") +
  coord_cartesian(ylim = c(10,80)) +
   ggtitle("C.")

Figure4.new <- confidenceVsVision.plot + visualMratio.marginal + 
  confidenceVsTAS.plot + summedTASScore.marginal + 
  VisionVsTAS.plot + 
  plot_layout(widths = c(4,1,4,1,4)) +
  plot_annotation(title = 'Participant-wise metacognitive measures')
Figure4.new

```


```{r correlateCovariates}

correlationBF(covariates$faces.confVsDist,       covariates$faces.similarityVsDist)
correlationBF(covariates$faces.confVsDist,       covariates$visual.Mratio)
correlationBF(covariates$faces.similarityVsDist, covariates$visual.Mratio)
correlationBF(covariates$faces.confVsDist,       covariates$summedTASScore)


correlationBF(covariates$faces.confVsDist,       covariates$visual.Mratio)
correlationBF(covariates$faces.similarityVsDist, covariates$visual.Mratio)
correlationBF(covariates$faces.confVsDist,       covariates$summedTASScore)
correlationBF(covariates$faces.similarityVsDist, covariates$summedTASScore)

cor.test(covariates$faces.confVsDist, covariates$summedTASScore)
cor.test(covariates$faces.similarityVsDist, covariates$summedTASScore)

similarityVsTAS <- lm(faces.similarityVsDist ~ summedTASScore, data=covariates)
anova(similarityVsTAS)
ggplot(covariates, aes(x=summedTASScore, y=faces.similarityVsDist)) +
  geom_point() +
  geom_smooth(method = 'lm', formula = y~x) 


confidenceVsTAS <- lm(faces.confVsDist ~ summedTASScore, data=covariates)
anova(confidenceVsTAS)
ggplot(covariates, aes(x=summedTASScore, y=faces.confVsDist)) +
  geom_point() +
  geom_smooth(method = 'lm', formula = y~x) 


```



