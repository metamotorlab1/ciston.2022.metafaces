
clear all
close all

dataPath = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment2/data/TAS';
addpath('/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment2/analysis_forPaper/TAS/jsonlab/')
outputPath = '/Users/elisa/Documents/_work/2_juniorGroup_BCCN/faces/Experiment2/analysis_forPaper/TAS/';

mkdir(outputPath);

%get experimental data (from all subjects at once)
datafile = dir([dataPath filesep 'TASresponses.txt']);
data = loadjson([dataPath filesep datafile.name]);

subj = 1; %correctly this would start from 0 but the first ID is missing
for resultID = 1:length(data)
    
    if length(data{resultID}) == 1 && length(data{resultID+1}) > 1
        %this is subjectID and is followed by actual result data
        subj = subj + 1;
        TAS.subjIDs{subj-1} = data{resultID}.ID;
    elseif length(data{resultID}) > 1
        for q = 1
            language{subj-1} = data{resultID}{1}.options;
        end
        %this is actual result Data
        for q = 1:length(data{resultID})
            responses(subj-1,q) = str2double(data{resultID}{q}.value);
            questionLabel{subj-1,q} = data{resultID}{q}.text;
        end

    end
end

TAS.subjIDs = TAS.subjIDs(:);

%invert item values
inverted_items = [5,9,11,12,13,15,16,21,24];
responses(:,inverted_items) = 6 - responses(:,inverted_items);

%subscores TAS

%scale 1: q: difficulties identifying feelings:
%items: 4,10,14,17,20,25,26
%no inverted items
%highest possible score = 35, lowest = 7
identifyfeel = responses(:,[4 10 14 17 20 25 26]);
TAS.scoreidentifyfeel = sum(identifyfeel,2);

%scale 2: difficulties describying feelings:
%items: 3,8,12,22,23
%12 inverted
%highest possible score = 25 lowest = 5 
describefeel = responses(:,[3 8 12 22 23]);
TAS.scoredescribefeel = sum(describefeel,2);

%scale 3: external orientated thinking:
%9,11,13, 15,21,24
%4 items inverted
%higher values mean external oriented thinking
%highest possible score = 30, lowest = 6

external = responses(:,[9 11 13 15 21 24]);
TAS.scoreexternal = sum(external,2);

%scale 4: daydreaming:
%2,5,16,18
%2 items inverted
%highest score = 20, lowest = 4
%higher score more daydreams, lower score no daydreams
daydream = responses(:,[2 5 16 18]);
TAS.scoredaydream = sum(daydream,2);

%total score: scale1+2+3 (18 items) highest possible score = 90, lowest possible score = 18
TAStotal = responses(:,[4 10 14 17 20 25 26 3 8 12 22 23 9 11 13 15 21 24]);
TAS.TASscore = sum(TAStotal,2);

struct2csv(TAS, [outputPath 'TASscore.csv'])
save([outputPath filesep 'TASscore.mat'], 'TAS')
